import React, { createContext, useContext, useState } from 'react';

const GlobalContext = createContext();

export const VirtualLibraryContext = ({ children }) => {
    const [bookInterfaceVisible, setBookInterfaceVisible] = useState(false);
    const [isSectionButtonVisible, setIsSectionButtonVisible] = useState(false);
    const [sectionInformation, setSectionInformation] = useState([]);
    const [userHeight, setUserHeight] = useState(1.7);
    const [libraryInfo, setLibraryInfo] = useState(null);

  return (
    <GlobalContext.Provider 
      value={{ bookInterfaceVisible, setBookInterfaceVisible,
              isSectionButtonVisible, setIsSectionButtonVisible,
              sectionInformation, setSectionInformation,
              userHeight, setUserHeight,
              libraryInfo, setLibraryInfo
             }}
    >
      {children}
    </GlobalContext.Provider>
  );
};

export const useGlobalContextLibrary = () => useContext(GlobalContext);
