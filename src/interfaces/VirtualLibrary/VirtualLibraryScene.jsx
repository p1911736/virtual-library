import { Canvas } from '@react-three/fiber';
import VirtualLibrary from './VirtualLibrary.jsx';
import React, { useEffect, useState } from 'react';
import { Vector3 } from 'three';
import GlobalInterface from '../../components/Interfaces/GlobalInterface.jsx';
import BookInterface from '../../components/Interfaces/BookInterface.jsx';
import BookButtonInterface from '../../components/Interfaces/BookButtonInterface.jsx';
import CameraSetup from '../../components/Configuration/CameraSetup.jsx';
import Controls from '../../components/Configuration/Controls.jsx';
import { OrbitControls } from '@react-three/drei';
import DeviceDetector from '../../components/Configuration/DeviceDetector.jsx';
import { useGlobalContextLibrary } from '../../interfaces/VirtualLibrary/VirtualLibraryContext.jsx';
import '../../css/VirtualLibrary/virtual-library.css';
import axios from 'axios';
import ApiService from '../../api/ApiService.js';

export default function VirtualLibraryScene() {
    const { userHeight, setUserHeight, libraryInfo, setLibraryInfo } = useGlobalContextLibrary();
    const [libraries, setLibraries] = useState([]);
    const [cameraPosition, setCameraPosition] = useState(null);
    const [cameraLookAt, setCameraLookAt] = useState(null);
    const [cameraFov, setCameraFov] = useState(45);
    const [updateCamera, setUpdateCamera] = useState(false);
    const spawnPosition = new Vector3(-4, 0, -15);
    const backgroundColor = '#f0f0f0';

    // Charger les bibliothèques
    useEffect(() => {
        axios.get(`${ApiService.getAPI_URL()}/api/libraries`)
            .then(response => {
            setLibraries(response.data);
            })
            .catch(error => {
            console.error('Error fetching libraries JSON:', error);
            });
    }, []);

    const handleLibraryChange = (event) => {
        const selectedLibraryId = parseInt(event.target.value, 10);
        const selectedLibrary = libraries.find(lib => lib.id === selectedLibraryId);
        setLibraryInfo({
            id: selectedLibraryId,
            name: selectedLibrary ? selectedLibrary.name : ''
        });
    };

    useEffect(() => {
        setUserHeight(1.75);
    }, [setUserHeight]);
    
    // useEffect(() => {
    //     setLibraryInfo({
    //         id: 38,
    //         name: 'Bibliothèque Sciences Lyon 1'
    //     });
    // }, []);
    
    const handleFloorClick = (floorPosition, lookAtPosition, fov) => {
        setCameraPosition(floorPosition);
        setCameraLookAt(lookAtPosition);
        setCameraFov(fov);
        setUpdateCamera(true);
    };

    return (
        <>
            {!libraryInfo || libraryInfo.id === null ? (
                <div className="form-container-library">
                    <label htmlFor="librarySelect">Sélectionnez une bibliothèque : </label>
                    <select id="librarySelect" onChange={handleLibraryChange}>
                        <option value="">-</option>
                        {libraries.map((library) => (
                            <option key={library.id} value={library.id}>
                                {library.name}
                            </option>
                        ))}
                    </select>
                </div>
            ) : (
                <>
                    <DeviceDetector>
                        {(isMobile) => (
                            <Canvas style={{ backgroundColor: backgroundColor }}
                                camera={{
                                    position: [spawnPosition.x, spawnPosition.y + userHeight, spawnPosition.z - 1],
                                    fov: 45,
                                    aspect: window.innerWidth / window.innerHeight,
                                    near: 0.1,
                                    far: 1000
                                }}
                                ratio={Math.min(window.devicePixelRatio, 2)}
                            >
                                {isMobile ? <OrbitControls /> : <OrbitControls />}
                                <VirtualLibrary />
                                {cameraPosition && updateCamera && (
                                    <CameraSetup 
                                        position={cameraPosition}
                                        lookAtPosition={cameraLookAt}
                                        fov={cameraFov}
                                        onUpdateComplete={() => setUpdateCamera(false)}
                                    />
                                )}
                            </Canvas>
                        )}
                    </DeviceDetector>

                    <GlobalInterface libraryId={libraryInfo.id} onFloorClick={handleFloorClick} />
                    <BookInterface />
                    <BookButtonInterface />
                </>
            )}
        </>
    );
}
