import React from 'react';
import Walls from '../../components/Structure/Walls.jsx';
import Carrels from '../../components/Structure/Carrels.jsx';
import Ground from '../../components/Structure/Ground.jsx';
import Bays from '../../components/Structure/Bays/Furnitures/Bays.jsx';
import Windows from '../../components/Structure/Windows/Windows.jsx';
import LevelText from '../../components/Texts/LevelText.jsx';
import Pillars from '../../components/Structure/Pillars.jsx';
import Itinerary from '../../components/Itinerary/Itinerary.jsx';
import { Perf } from 'r3f-perf';
import DynamicSections from '../../components/Structure/Bays/Sections/DynamicSections.jsx';

export default function VirtualLibrary() {

  return (
    <>
      {/* <Perf position='top-left'/> */}

      {/* Lumières */}
      <directionalLight intensity={1.0} />
      <ambientLight intensity={0.5} />

      {/* Structure */}
      <Walls />
      <Carrels />
      <Ground />
      <Pillars />
        
      {/* Textes */}
      {/* <LevelText /> */}

      {/* Objets */}
      <Windows />
      <Bays />
      <DynamicSections />
      
      {/* Itineraire */}
      <Itinerary />
    </>
  );
}


