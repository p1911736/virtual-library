import React from "react";
import DynamicForm from "../../../components/Forms/DynamicForm";

export default function ModelEdition({ json, currentPage })
{
    const titleKeyMap = {
        fieldsBookshelfModels: 'Étagères',
        fieldsWallModels: 'Murs',
        fieldsWindowModels: 'Fenêtres',
    };
    
    return (
        <>
            <h1>{titleKeyMap[currentPage]}</h1>
            <DynamicForm fields={json} />
        </>
    );
}