import React from 'react';

export default function ModelEditionNavigation( { setCurrentPage }) 
{
    return (
        <nav style={{ display: 'flex', justifyContent: 'flex-end', padding: '10px' }}>
            <button 
                onClick={() => setCurrentPage('fieldsBookshelfModels')} 
            >
                Étagères
            </button>
            <button 
                onClick={() => setCurrentPage('fieldsWallModels')} 
            >
                Murs
            </button>
            <button 
                onClick={() => setCurrentPage('fieldsWindowModels')} 
            >
                Fenêtres
            </button>
        </nav>
    );
}