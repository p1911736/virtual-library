import React, { useState, useEffect } from 'react';
import ModelEditionNavigation from './ModelEditionNavigation.jsx';
import ModelEdition from './ModelEdition.jsx';

export default function ModelEditionScene() {
    const [currentPage, setCurrentPage] = useState(null);
    const [modelJson, setModelJson] = useState({});

    useEffect(() => {
        async function loadJsonFiles() {
            const importedModules = null; //await importAllJsonFiles();
            const jsonObjects = {};
            
            for (const key in importedModules) {
                jsonObjects[key] = importedModules[key].default;
            }

            setModelJson(jsonObjects);
        }

        loadJsonFiles();
    }, []);

    const renderScene = () => {
        if (!currentPage || !modelJson[currentPage]) {
            return null;
        }
    
        return <ModelEdition json={modelJson[currentPage]} currentPage={currentPage} />;
    };

    return (
        <>
            <ModelEditionNavigation setCurrentPage={setCurrentPage} />
            {renderScene()}
        </>
    );
}
