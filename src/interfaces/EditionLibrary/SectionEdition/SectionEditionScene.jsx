import React, { useState, useEffect } from 'react';
import { Canvas } from '@react-three/fiber';
import SectionEdition from './SectionEdition.jsx';
import CameraSetup from '../../../components/Configuration/CameraSetup.jsx';
import DynamicForm from '../../../components/Forms/DynamicForm.jsx';
import sectionFieldsAddition from '../../../json/fieldsSections/fieldsSections-addition.json';
import '../../../css/EditionLibrary/SectionEdition/sectionEdition.css';
import { getMaximalSubsections } from '../../../utils/jsonUtils.js';
import { useGlobalContextEdition } from '../../EditionLibrary/SectionEdition/SectionEditionContext.jsx';
import axios from 'axios';
import ApiService from '../../../api/ApiService.js';

export default function SectionEditionScene() 
{
    const [bayData, setBayData] = useState([]);
    const { libraryInfo, setLibraryInfo } = useGlobalContextEdition();
    const [sectionData, setSectionData] = useState(null);
    const [libraries, setLibraries] = useState([]);
    const [floorData, setFloorData] = useState(0);
    const backgroundColor = '#f0f0f0';

    // Charger les bibliothèques
    useEffect(() => {
        axios.get(`${ApiService.getAPI_URL()}/api/libraries`)
            .then(response => {
            setLibraries(response.data);
            })
            .catch(error => {
            console.error('Error fetching libraries JSON:', error);
            });
    }, []);

    // Charger les données des bays
    useEffect(() => {
        if (libraryInfo) {
            axios.get(`${ApiService.getAPI_URL()}/api/bays/${libraryInfo.id}`)
                .then(response => {
                setBayData(response.data);
                })
                .catch(error => {
                console.error('Error fetching bays JSON:', error);
                });
        }
    }, [libraryInfo]);

    // Charger les données des sections
    useEffect(() => {
        if (libraryInfo) {
            axios.get(`${ApiService.getAPI_URL()}/api/sections/${libraryInfo.id}`)
                .then(response => {
                    setSectionData(response.data);
                })
                .catch(error => {
                    console.error('Error fetching sections JSON:', error);
                });
        }
    }, [libraryInfo]);

    // Charger le nombre d'étages
    useEffect(() => {
        if (libraryInfo)
        {
          axios.get(`${ApiService.getAPI_URL()}/api/floors/${libraryInfo.id}`)
            .then(response => {
              const floors = Array.from({ length: response.data.length }, (_, i) => ({ floor: i }));
              setFloorData(floors);
            })
            .catch(error => {
              console.error('Error fetching floors JSON:', error);
            });
        }
      }, [libraryInfo]);

    const handleLibraryChange = (event) => {
        const selectedLibraryId = parseInt(event.target.value, 10);
        const selectedLibrary = libraries.find(lib => lib.id === selectedLibraryId);
        setLibraryInfo({
            id: selectedLibraryId,
            name: selectedLibrary ? selectedLibrary.name : ''
        });
    };
    
    useEffect(() => {
        setLibraryInfo({
            id: 37,
            name: 'Bibliothèque Mathématiques'
        });
    }, []);

    return (
        <>
        {!libraryInfo || libraryInfo.id === null ? (
                <div className="form-container-library">
                    <label htmlFor="librarySelect">Sélectionnez une bibliothèque : </label>
                    <select id="librarySelect" onChange={handleLibraryChange}>
                        <option value="">-</option>
                        {libraries.map((library) => (
                            <option key={library.id} value={library.id}>
                                {library.name}
                            </option>
                        ))}
                    </select>
                </div>
            ) : (
                <>
                    <Canvas style={{backgroundColor: backgroundColor, height: '35vh'}}
                        camera={{
                            position: [0, 0, -1],
                            fov: 45,
                            aspect: window.innerWidth / window.innerHeight,
                            near: 0.1,
                            far: 300
                        }}
                        ratio={Math.min(window.devicePixelRatio, 2)}
                    >
                        <SectionEdition />
                    </Canvas>
                    <div className="form-container-edition">
                        {sectionData && sectionData !== null && <DynamicForm fields={sectionFieldsAddition} selectData={[ bayData, getMaximalSubsections(sectionData), floorData ]} /> }
                    </div>
                </>
            )}
        </>
    );
}