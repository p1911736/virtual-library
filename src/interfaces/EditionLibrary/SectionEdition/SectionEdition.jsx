import React, { useEffect, useState, useMemo } from 'react';
import { accumulatedModelsWidth, maxModelsHeight } from '../../../utils/dataUtils.js';
import Bay from '../../../components/Structure/Bays/Furnitures/Bay.jsx';
import { useGlobalContextEdition } from './SectionEditionContext.jsx';
import CameraSetup from '../../../components/Configuration/CameraSetup.jsx';
import { Vector3 } from 'three';
import Sections from '../../../components/Structure/Bays/Sections/Sections.jsx';
import { OrbitControls } from '@react-three/drei';
import { generateLightColor } from '../../../utils/calculateUtils.js';
import { findSectionsByBay } from '../../../utils/jsonUtils.js';
import { v4 as uuidv4 } from 'uuid';

export default function SectionEdition() {
    const { currentBayData, currentSectionData, libraryInfo } = useGlobalContextEdition();
    const [bayData, setBayData] = useState(null);
    const [sections, setSections] = useState([]);
    const sectionColor = useMemo(() => generateLightColor(), []);
    const [selectedSectionName, setSelectedSectionName] = useState(null);
    const [sectionExists, setSectionExists] = useState(false);
    const [prevSelectedSection, setPrevSelectedSection] = useState(null);

    // Mise à jour des données de la travée
    useEffect(() => {
        if (currentBayData) {
            const modelsWidth = accumulatedModelsWidth(currentBayData.models);
            const maxHeight = maxModelsHeight(currentBayData.models);
            const startingPosition = new Vector3(-modelsWidth / 2, 0, 0);
            const endPosition = new Vector3(modelsWidth / 2, 0, 0);

            const newBayData = {
                ...currentBayData,
                starting_position: startingPosition,
                end_position: endPosition,
                max_models_height: maxHeight
            };
            setBayData(newBayData);

            const sectionsByBay = findSectionsByBay(currentBayData.id_bay_osm);
            setSections(sectionsByBay.map(section => ({
                ...section,
                bay: newBayData,
                color: generateLightColor(),
                key: uuidv4()
            })));
        } else {
            setBayData(null);
            setSections([]);
        }
    }, [currentBayData]);

    // Mettre à jour sectionExists lorsque la section sélectionnée change
    useEffect(() => {
        if (currentBayData && currentSectionData && currentSectionData.selectedSection) {
            if (prevSelectedSection?.name !== currentSectionData.selectedSection.name || prevSelectedSection?.bay.id_bay_osm !== bayData?.id_bay_osm) {
                const exists = sections.some(section => section.name === currentSectionData.selectedSection.name && section.bay.id_bay_osm === bayData?.id_bay_osm);
                setSectionExists(exists);
                setPrevSelectedSection(currentSectionData.selectedSection);
            }
        } else {
            setSectionExists(false);
            setPrevSelectedSection(null);
        }
    }, [currentSectionData, sections, bayData]);

    // Mise à jour des données de la section
    useEffect(() => {
        if (currentSectionData && bayData) {
            const slidersCondition = currentSectionData["slider-line1"] + 5 < currentSectionData["slider-line2"] - 5;
            if (currentSectionData.selectedSection) {
                setSelectedSectionName(currentSectionData.selectedSection.name);
                const columnCondition = currentSectionData.column1 <= currentSectionData.column2;

                currentSectionData.line1 = parseInt(currentSectionData.line1);
                currentSectionData.line2 = parseInt(currentSectionData.line2);

                const callNumber = currentSectionData.selectedSection ? currentSectionData.selectedSection.call_number : 0;
                const name = currentSectionData.selectedSection ? currentSectionData.selectedSection.name : '';

                const newSection = {
                    bay: bayData,
                    call_number: callNumber,
                    name: name,
                    start_column: parseInt(currentSectionData.column1),
                    end_column: parseInt(currentSectionData.column2),
                    start_line: `${currentSectionData["slider-line1"]}:${currentSectionData.line1}`,
                    end_line: `${currentSectionData["slider-line2"]}:${currentSectionData.line2}`,
                    color: sectionColor,
                    key: uuidv4()
                };

                if (columnCondition && slidersCondition && newSection.name) {
                    setSections(prevSections => {
                        if (sectionExists) {
                            return prevSections.map(section =>
                                section.name === newSection.name && section.bay.id_bay_osm === newSection.bay.id_bay_osm
                                    ? { ...section, ...newSection }
                                    : section
                            );
                        } else {
                            return [
                                ...prevSections,
                                newSection
                            ];
                        }
                    });
                }
            } else {
                if (slidersCondition) {
                    setSections(prevSections => {
                        return prevSections.filter(section => section.name !== selectedSectionName);
                    });
                }
            }
        }
    }, [currentSectionData, bayData, sectionColor, selectedSectionName, sectionExists]);

    return (
        <>
            {/* Lumières */}
            <directionalLight intensity={1} />
            <ambientLight intensity={0.5} />

            <OrbitControls />

            {/* Travée */}
            {bayData && (
                <Bay bay={bayData} />
            )}
            {bayData && (
                <CameraSetup
                    position={[0, bayData.max_models_height / 2, bayData.starting_position.z + bayData.max_models_height * 1.5]}
                    lookAtPosition={[0, bayData.max_models_height / 2, bayData.starting_position.z]}
                />
            )}
            
            {/* Affichage des sections */}
            {bayData && sections.length > 0 && (
                <Sections sections={sections} isPreview={true} libraryId={libraryInfo.id} />
            )}
        </>
    );
}
