import React, { createContext, useContext, useState } from 'react';

const GlobalContext = createContext();

export const SectionEditionContext = ({ children }) => {
    const [currentBayData, setCurrentBayData] = useState(null);
    const [currentSectionData, setCurrentSectionData] = useState(null);
    const [libraryInfo, setLibraryInfo] = useState(null);

  return (
    <GlobalContext.Provider 
      value={{ currentBayData, setCurrentBayData,
        currentSectionData, setCurrentSectionData,
        libraryInfo, setLibraryInfo
       }}
    >
      {children}
    </GlobalContext.Provider>
  );
};

export const useGlobalContextEdition = () => useContext(GlobalContext);
