import React, { useState } from 'react';
import Navigation from './Navigation.jsx';
import { VirtualLibraryContext } from './VirtualLibrary/VirtualLibraryContext.jsx';
import { SectionEditionContext } from './EditionLibrary/SectionEdition/SectionEditionContext.jsx';
import VirtualLibraryScene from './VirtualLibrary/VirtualLibraryScene.jsx';
import SectionEditionScene from './EditionLibrary/SectionEdition/SectionEditionScene.jsx';
import ModelEditionScene from './EditionLibrary/ModelEdition/ModelEditionScene.jsx';
import DeviceDetector from '../components/Configuration/DeviceDetector.jsx';

export default function Pages() {
    const [currentPage, setCurrentPage] = useState('VirtualLibrary');

    const renderScene = () => {
        switch (currentPage) {
            case 'VirtualLibrary':
                return (
                    <>
                        <VirtualLibraryContext>
                            <VirtualLibraryScene />;
                        </VirtualLibraryContext>
                    </>
                )
            case 'SectionEdition':
                return (
                    <>
                        <VirtualLibraryContext>
                            <SectionEditionContext>
                                <SectionEditionScene />
                            </SectionEditionContext>
                        </VirtualLibraryContext>
                    </>
                )
            case 'ModelEdition':
                return (
                    <>
                        <SectionEditionContext>
                            <ModelEditionScene />
                        </SectionEditionContext>
                    </>
                )
            default:
                return null;
        }
    };

    return (
        <DeviceDetector>
            {(isMobile) => (
                <>
                    {/* Décommentez ici pour avoir la rubrique pour naviguer dans l'édition des sections */}
                    {!isMobile && <Navigation setCurrentPage={setCurrentPage} />}
                    {renderScene()}
                </>
            )}
        </DeviceDetector>
    );
}
