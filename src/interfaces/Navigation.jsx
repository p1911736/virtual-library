import React from 'react';

export default function Navigation({ setCurrentPage }) {
    return (
        <nav style={{ display: 'flex', justifyContent: 'flex-end', padding: '10px', backgroundColor: '#f0f0f0' }}>
            <button 
                onClick={() => setCurrentPage('VirtualLibrary')} 
                style={{ marginLeft: '10px' }}
            >
                Bibliothèque
            </button>
            <button 
                onClick={() => setCurrentPage('SectionEdition')} 
                style={{ marginLeft: '10px' }}
            >
                Édition des sections
            </button>
            <button 
                onClick={() => setCurrentPage('ModelEdition')} 
                style={{ marginLeft: '10px' }}
            >
                Édition des étagères
            </button>
        </nav>
    );
}
