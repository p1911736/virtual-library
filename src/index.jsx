import './style.css'
import ReactDOM from 'react-dom/client'
import React, { StrictMode } from 'react';
import Pages from './interfaces/Pages.jsx';

const root = ReactDOM.createRoot(document.querySelector('#root'))

root.render(
    <StrictMode>
        <Pages />
    </StrictMode>
)