import { Vector3, Matrix4, Euler } from 'three';
import { epsilon } from '../utils/constants.js';
import { getBayAttributes } from './dataUtils.js';

export function gpsCoordinatesTo3D(coordinates, refLatitude, refLongitude) 
{
    // Convertir latitude et longitude en radians
    const [latitude, longitude] = coordinates.split(',').map(Number);
    const latRad = latitude * Math.PI / 180;
    const lonRad = longitude * Math.PI / 180;
    
    // Point de référence en radians
    const refLatRad = refLatitude * Math.PI / 180;
    const refLonRad = refLongitude * Math.PI / 180;

    // Rayon moyen de la Terre en mètres
    const R = 6371000; 
    
    // Calcul des coordonnées x et z avec projection équirectangulaire
    const x = R * (lonRad - refLonRad) * Math.cos(refLatRad);
    const z = R * (latRad - refLatRad);

    return {
        "x": x,
        "y": 0, // 'y' reste à 0 comme demandé
        "z": z
    };
}



export function calculateAveragePosition(position1, position2) 
{
  return new Vector3(
    (position1.x + position2.x) / 2,
    (position1.y + position2.y) / 2,
    (position1.z + position2.z) / 2
  );
}

export function calculateCentroid(coordinates) 
{
  if (coordinates.length === 0) {
    return null;
  }

  let sumX = 0;
  let sumY = 0;
  let sumZ = 0;

  coordinates.forEach(coord => {
    sumX += coord.x;
    sumY += coord.y;
    sumZ += coord.z;
  });

  const avgX = sumX / coordinates.length;
  const avgY = sumY / coordinates.length;
  const avgZ = sumZ / coordinates.length;

  return new Vector3(avgX, avgY, avgZ);
}

export function calculateAttributesBetweenPoints(pointA, pointB, height) 
{
  const length = pointA.distanceTo(pointB);
  const position = new Vector3().copy(pointA).lerp(pointB, 0.5);
  position.y += height / 2;

  const rotationMatrix = new Matrix4();
  rotationMatrix.lookAt(pointA, pointB, new Vector3(0, 1, 0));
  const rotation = new Euler().setFromRotationMatrix(rotationMatrix);

  return { length, position, rotation };
}
  
export function calculateGroundSize(groundPoints) 
{
  let minX = Infinity;
  let minZ = Infinity;
  let maxX = -Infinity;
  let maxZ = -Infinity;

  groundPoints.forEach(point => {
    minX = Math.min(minX, point.x);
    minZ = Math.min(minZ, point.z);
    maxX = Math.max(maxX, point.x);
    maxZ = Math.max(maxZ, point.z);
  });

  const width = maxX - minX;
  const depth = maxZ - minZ;

  const position = [(minX + maxX) / 2, groundPoints[0].y - epsilon, (minZ + maxZ) / 2];

  return { width, depth, position };
}

export function splitWithPattern(inputString, pattern) 
{
  const splitStrings = inputString.split(';');

  return splitStrings.map((str, index) => 
  {
      const match = str.match(pattern);
      if (!match) 
      {
          console.error(`Pattern "${pattern}" non trouvé dans la chaîne "${str}".`);
          return null;
      }

      const [, ...matches] = match;
      return matches;
  });
}

export function splitRange(range) 
{
  return range.split(":").map(parseFloat);
}

export function calculateDirection(bay)
{
  return bay.starting_position.x < bay.end_position.x ? 1 : -1;
}

export function darkenHexColor(hex, factor) 
{
  // Convertir la couleur hexadécimale en RVB
  const bigint = parseInt(hex.slice(1), 16);
  const r = (bigint >> 16) & 255;
  const g = (bigint >> 8) & 255;
  const b = bigint & 255;

  // Appliquer le facteur d'assombrissement
  const dr = Math.round(r * factor);
  const dg = Math.round(g * factor);
  const db = Math.round(b * factor);

  // Retourner la couleur assombrie au format hexadécimal
  return `#${(dr << 16 | dg << 8 | db).toString(16).padStart(6, '0')}`;
}

export function lightenHexColor(hex, factor) {
  // Convertir la couleur hexadécimale en RVB
  const bigint = parseInt(hex.slice(1), 16);
  const r = (bigint >> 16) & 255;
  const g = (bigint >> 8) & 255;
  const b = bigint & 255;

  // Appliquer le facteur d'éclaircissement
  const lr = Math.min(255, Math.round(r + (255 - r) * factor));
  const lg = Math.min(255, Math.round(g + (255 - g) * factor));
  const lb = Math.min(255, Math.round(b + (255 - b) * factor));

  // Retourner la couleur éclaircie au format hexadécimal
  return `#${(lr << 16 | lg << 8 | lb).toString(16).padStart(6, '0')}`;
}

export function calculateCameraBayDistance(cameraPosition, bays, bay) {
  const bayAttributes = getBayAttributes(bays, bay);

  const { starting_position, end_position } = bayAttributes[0];

  const bayPosition = new Vector3().copy(starting_position).lerp(end_position, 0.5);

  return cameraPosition.distanceTo(bayPosition);
}

export function convertCoordinatesToVector3(data) {
  return data.map(element => ({
    ...element,
    coordinates: element.coordinates.map(coord => {
      if (coord instanceof Vector3) {
        return coord;
      } else {
        return new Vector3(coord.x, coord.y, coord.z);
      }
    })
  }));
};

export function generateLightColor() {
  const r = Math.floor(200 + Math.random() * 55); // 200-255
  const g = Math.floor(200 + Math.random() * 55); // 200-255
  const b = Math.floor(200 + Math.random() * 55); // 200-255
  return `#${((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1)}`;
}
