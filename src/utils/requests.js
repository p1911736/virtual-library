import express from 'express';
import fs from 'fs';
import { Vector3 } from 'three';
import { buildSectionHierarchy, updateSectionData } from './jsonUtils.js';
import { gpsCoordinatesTo3D } from './calculateUtils.js';
import path from 'path';
import { LocalStorage } from 'node-localstorage';
const localStorage = new LocalStorage('./scratch');

const router = express.Router();

const basePath = path.resolve('./src/json/libraries');

/*
----- DONNÉES DE LA BIBLIOTHÈQUE -----

    Types possibles :
    - sections
    - floors (étages)
    - grounds (sol)
    - pillars
    - walls
    - windows
    - bays
*/

router.post('/update-library/:libraryId', (req, res) => {
    const libraryData = req.body;
    const { libraryId } = req.params;

    const objectData = libraryData[0].objects;
    const bayData = libraryData[1].bays;

    // Chemin vers le fichier floors.json
    const jsonFilePath = `./src/json/libraries/library_${libraryId}/floors.json`;

    // Lire le fichier floors.json pour récupérer les données des étages
    let floorsData;
    try {
        floorsData = JSON.parse(fs.readFileSync(jsonFilePath, { encoding: 'utf8', flag: 'r' }));
    } catch (error) {
        console.log('Error reading floors.json:', error);
        return res.status(500).send('Error reading floors data');
    }

    const refLatitude = 45.7819;
    const refLongitude = 4.8706;

    // Transformation des données pour les travées
    const transformedBayData = bayData.map(bay => {
        const level = parseInt(bay.id_travee_bu.split('.')[0], 10);
        const floorData = floorsData.find(floor => floor.level === level);
        
        if (!floorData) {
            console.log(`No floor data found for level ${level}`);
            return null;
        }
        
        const yPosition = floorData["3d_position"].y;
        const startingPosition = gpsCoordinatesTo3D(bay.coordonnees_depart, refLatitude, refLongitude);
        startingPosition.y = yPosition;

        const endPosition = gpsCoordinatesTo3D(bay.coordonnees_arrivee, refLatitude, refLongitude);
        endPosition.y = yPosition;

        return {
            "id_bay_osm": bay.id_travee_bu,
            "models": bay.modeles_rayonnages,
            "starting_position": startingPosition,
            "end_position": endPosition
        };
    }).filter(bay => bay !== null); // Filtrer les valeurs null

    // Transformation des données pour les objets
    const transformedObjects = objectData.map(object => {
        const level = parseInt(object.id_etage, 10);
        const floorData = floorsData.find(floor => floor.level === level);
        
        if (!floorData) {
            console.log(`No floor data found for level ${level}`);
            return null;
        }

        const yPosition = floorData["3d_position"].y;
        const coordinatesArray = object.coordonnees.split(';').map(coord => {
            const position = gpsCoordinatesTo3D(coord, refLatitude, refLongitude);
            position.y = yPosition;
            return position;
        });

        return {
            "type_objet": object.type_objet,
            "model": object.modele_objet,
            "positions": coordinatesArray
        };
    }).filter(object => object !== null); // Filtrer les valeurs null

    // Séparer les objets par type et les préparer pour l'écriture dans les fichiers appropriés
    const windowsData = transformedObjects
        .filter(obj => obj.type_objet === 'window')
        .map(window => ({
            "models": window.model,
            "starting_position": window.positions[0],  // Position de départ
            "end_position": window.positions[1]  // Position de fin (supposé qu'il y a deux coordonnées)
        }));

    const carrelsData = transformedObjects
        .filter(obj => obj.type_objet === 'carrel')
        .map(carrel => ({
            "model": carrel.model,
            "coordinates": carrel.positions
        }));

    const pillarsData = transformedObjects
        .filter(obj => obj.type_objet === 'column')
        .map(pillar => ({
            "model": pillar.model,
            "coordinates": pillar.positions
        }));

    try {
        fs.writeFileSync(`./src/json/libraries/library_${libraryId}/bays.json`, JSON.stringify(transformedBayData, null, 2));
        fs.writeFileSync(`./src/json/libraries/library_${libraryId}/windows.json`, JSON.stringify(windowsData, null, 2));
        fs.writeFileSync(`./src/json/libraries/library_${libraryId}/carrels.json`, JSON.stringify(carrelsData, null, 2));
        fs.writeFileSync(`./src/json/libraries/library_${libraryId}/pillars.json`, JSON.stringify(pillarsData, null, 2));
        return res.status(200).json({ bays: transformedBayData, windows: windowsData, carrels: carrelsData, pillars: pillarsData });
    } catch (error) {
        console.log('Error writing JSON files:', error);
        return res.status(500).send('Error while updating library data');
    }
});


// Récupère des données dans la bibliothèque
router.get('/:type/:libraryId', (req, res) => {
    const { type, libraryId } = req.params;

    const jsonFilePath = path.join(basePath, `library_${libraryId}/${type}.json`);

    try {
        const jsonData = JSON.parse(fs.readFileSync(jsonFilePath, { encoding: 'utf8', flag: 'r' }));
        
        return res.status(200).json(jsonData);
    } catch (error) {
        console.error('Error reading/parsing JSON file:', error);
        return res.status(500).send('Error reading/parsing JSON file');
    }
});

// Met à jour des données dans la bibliothèque
router.post('/update/:type', (req, res) => 
{
    const { type } = req.params;
    const elementData = req.body;
    
    const elements = buildSectionHierarchy(elementData.elements);
    
    try {
        fs.writeFileSync(`./src/json/libraries/library_${elementData.id_library}/${type}.json`, JSON.stringify(elements, null, 2));
        return res.status(200).json(elements);
    }
    catch (error) {
        console.log(error)
        return res.status(500).send('Error while updating section data');
    }
});

// Récupère les id et noms de bibliothèques
router.get('/libraries', (req, res) => {
    const { type, libraryId } = req.params;

    const jsonFilePath = path.join(basePath, 'libraries.json');

    try {
        const jsonData = JSON.parse(fs.readFileSync(jsonFilePath, { encoding: 'utf8', flag: 'r' }));
        
        return res.status(200).json(jsonData);
    } catch (error) {
        console.error('Error reading/parsing JSON file:', error);
        return res.status(500).send('Error reading/parsing JSON file');
    }
});


/*
----- SECTIONS DANS LA BIBLIOTHÈQUE -----
*/

// Met à jour le placement des sections après l'utilisation de l'interface du backend d'Opale
router.post('/update-section-placement', (req, res) =>
{
    const sectionPlacementData = req.body;
    try {
        let sectionsData = JSON.parse(fs.readFileSync('./src/json/sections/sections_38.json', { encoding: 'utf8', flag: 'r' }));

        sectionsData = updateSectionData(sectionsData, sectionPlacementData);
        fs.writeFileSync('./src/json/sections_38.json', JSON.stringify(sectionsData, null, 2));

        return res.status(200).json(sectionsData);
    }
    catch (error) {
        console.log(error)
        return res.status(500).send('Error while writing placement section data');
    }
});


/*
----- LIVRE ACTUEL RECHERCHÉ -----
*/

// Mise à jour du livre actuel recherché avec des sessions
router.post('/set-current-book', (req, res) => {
    localStorage.setItem('bookData', JSON.stringify([...req.body, {model: 'A'} ]));
    res.status(200).send({message: 'Data set'});
});

// Récupère le livre actuel recherché avec des sessions
router.get('/get-current-book', (req, res) => {
    const bookData = JSON.parse(localStorage.getItem('bookData'));
    if (bookData) {
        res.status(200).send(bookData);
    } else {
        res.status(404).send({ message: 'No book data found' });
    }
});


export default router;