export const epsilon = 0.01; // Valeur de surplus pour éviter les problèmes de z-fighting 
export const scale = 1; // Échelle de la maquette

{ /* LIVRE */ }
export const bookParameters = {
    width: 0.03,
    spacing: 0
};