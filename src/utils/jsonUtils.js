import data from '../json/libraries/library_38/sections.json' assert { type: "json" };
import { lightenHexColor } from './calculateUtils.js';
import * as XLSX from 'xlsx';
import fs from 'fs/promises';

/* Sections json functions */

export function getMaximalSubsections(data) {
    let sections = [];

    data.forEach(section => {
        // Ajouter la section actuelle à la liste des sections
        sections.push(section);

        // Vérifier s'il existe des sous-sections
        if (section.subsections && section.subsections.length > 0) {
            const subsections = getMaximalSubsections(section.subsections);
            sections = sections.concat(subsections);
        }
    });

    return sections;
}

export function findSectionsByBay(bay, sections = data) {
    let result = [];

    sections.forEach(section => {
        const placementConditon = section.bay && section.start_line && section.end_line && section.start_column && section.end_column;
        if (section.bay === bay && placementConditon) {
            result.push(section);
        }

        if (section.subsections && section.subsections.length > 0) {
            result = result.concat(findSectionsByBay(bay, section.subsections));
        }
    });

    return result;
}

export function filterSections(sections, condition) 
{
    let filteredSections = [];

    sections.forEach(section => {
        let meetsCriteria = true;

        // Vérifie chaque critère dans condition
        for (let key in condition) {
            // Si la section ne possède pas l'attribut spécifié dans condition ou si sa valeur ne correspond pas à celle spécifiée, la section ne répond pas aux critères
            if (!(key in section) || (condition[key] && !section[key])) {
                meetsCriteria = false;
                break;
            }
        }

        // Si la section répond à tous les critères, l'ajouter à filteredSections
        if (meetsCriteria) 
        {
            filteredSections.push(section);
        }

        // Si la section a des sous-sections, filtre les sous-sections récursivement
        if (section.subsections && section.subsections.length > 0) 
        {
            filteredSections.push(...filterSections(section.subsections, condition));
        }
    });

    return filteredSections;
}


function findSection(sections, callNumberParts) 
{
    for (const section of sections) 
    {
        // Décompose la cote
        const sectionParts = section.call_number;
        
        // Compare la cote de la section actuel avec la cote des autres section
        let match = true;
        for (let i = 0; i < sectionParts.length; i++) 
        {
            if (callNumberParts[i] !== sectionParts[i]) 
            {
                match = false;
                break;
            }
        }
        
        if (match) 
        {
            // Si la section est la section recherchée
            if (sectionParts.length === callNumberParts.length) 
            {
                return section;
            }

            // Si la section a des sous-sections
            if (section.subsections) 
            {
                return findSection(section.subsections, callNumberParts);
            } 
            else 
            {
                // Si la section n'a pas de sous-sections et que la cote de la section est plus courte que la cote recherchée
                if (sectionParts.length < callNumberParts.length) 
                {
                    if (!section.subsections) 
                    {
                        section.subsections = [];
                    }
                    return section;
                }
            }
        }
    }
    return null;
}

function findParentSection(sections, callNumberParts) 
{
    // Vérifie qu'il y a plus d'un élément dans la cote
    if (callNumberParts.length > 1) 
    { 
        const parentCallNumber = callNumberParts.slice(0, -1); // Retire le dernier élément de la cote
        return findSection(sections, parentCallNumber);
    } 
    else 
    {
        return null; // Aucune section parente trouvée si le numéro d'appel est vide ou ne contient qu'un élément
    }
}

export function addSubsection(callNumber, name) 
{
    // Trouver la section parent appropriée
    const parentSection = findParentSection(data, callNumber);

    if (parentSection) 
    {
        // Ajout de la nouvelle section
        const newSubsection = {
            call_number: callNumber,
            name: name,
            subsections: [] 
        };

        parentSection.subsections.push(newSubsection);
    }
}

export function extractSectionsAtLevel(sections, level, currentLevel = 0) 
{
    if (currentLevel === level) 
    {
        return sections;
    }

    let result = [];
    sections.forEach(section => {
        if (section.subsections && section.subsections.length > 0) 
        {
            result = result.concat(extractSectionsAtLevel(section.subsections, level, currentLevel + 1));
        }
    });
    return result;
}
function splitRange(range) {
    const match = range.match(/(\d*\.*\d*):(\d*)/);
    if (!match) {
        throw new Error(`Invalid range format: ${range}`);
    }
    const [_, startPosition, lineNumber] = match;
    return [parseFloat(startPosition), parseInt(lineNumber)];
}

function aggregateSubsectionInfo(subsections) {
    const bayInfo = {};

    subsections.forEach(subsection => {
        if (subsection.bay) {
            if (!bayInfo[subsection.bay]) {
                bayInfo[subsection.bay] = {
                    startLinePosition: null,
                    startLineNumber: null,
                    endLinePosition: null,
                    endLineNumber: null,
                    startColumn: null,
                    endColumn: null
                };
            }

            const info = bayInfo[subsection.bay];

            if (subsection.start_line) {
                try {
                    const [position, lineNumber] = splitRange(subsection.start_line);
                    if (info.startLineNumber === null || lineNumber < info.startLineNumber || (lineNumber === info.startLineNumber && position < info.startLinePosition)) {
                        info.startLinePosition = position;
                        info.startLineNumber = lineNumber;
                    }
                } catch (error) {
                    console.error(error.message);
                }
            }

            if (subsection.end_line) {
                try {
                    const [position, lineNumber] = splitRange(subsection.end_line);
                    if (info.endLineNumber === null || lineNumber > info.endLineNumber || (lineNumber === info.endLineNumber && position > info.endLinePosition)) {
                        info.endLinePosition = position;
                        info.endLineNumber = lineNumber;
                    }
                } catch (error) {
                    console.error(error.message);
                }
            }

            if (subsection.start_column !== undefined) {
                if (info.startColumn === null || subsection.start_column < info.startColumn) {
                    info.startColumn = subsection.start_column;
                }
            }

            if (subsection.end_column !== undefined) {
                if (info.endColumn === null || subsection.end_column > info.endColumn) {
                    info.endColumn = subsection.end_column;
                }
            }
        }
    });

    return bayInfo;
}

function splitSectionsByBays(section, bayInfo) {
    return Object.keys(bayInfo).map(bay => ({
        ...section,
        start_line: bayInfo[bay].startLinePosition !== null ? `${bayInfo[bay].startLinePosition}:${bayInfo[bay].startLineNumber}` : null,
        end_line: bayInfo[bay].endLinePosition !== null ? `${bayInfo[bay].endLinePosition}:${bayInfo[bay].endLineNumber}` : null,
        start_column: bayInfo[bay].startColumn,
        end_column: bayInfo[bay].endColumn,
        bay,
        subsections: section.subsections.filter(subsec => subsec.bay === bay).map(subsec => ({
            ...subsec,
            bay: undefined 
        }))
    }));
}

export function fillSectionInfo(sections) {
    let result = [];
    sections.forEach(section => {
        if (section.subsections && section.subsections.length > 0) {
            const filledSubsections = fillSectionInfo(section.subsections);
            const bayInfo = aggregateSubsectionInfo(filledSubsections);

            filledSubsections.forEach(subsection => {
                if (!subsection.bay) {
                    const bay = Object.keys(bayInfo).find(b => {
                        return filledSubsections.some(subsec => subsec.bay === b);
                    });
                    subsection.bay = bay;
                }
            });

            section = {
                ...section,
                subsections: filledSubsections
            };

            const splitSections = splitSectionsByBays(section, bayInfo);
            result.push(...splitSections);
        } else {
            result.push(section);
        }
    });
    return result;
}


// Fonction récursive pour assigner la couleur aux sections et leurs sous-sections
export function assignColorToSections(sections) 
{
    function assignColor(section, parentColor, level) 
    {
        const currentColor = section.color || parentColor;
        const lightenedColor = lightenHexColor(currentColor, level * 0.03);
        
        section.color = lightenedColor;

        if (section.subsections) {
            section.subsections.forEach(subsection => {
                assignColor(subsection, lightenedColor, level + 1);
            });
        }
    }

    sections.forEach(section => {
        assignColor(section, section.color || '#000000', 0);
    });

    return sections;
}

export function updateSectionData(sections, sectionPlacementData) {
    const sectionData = sectionPlacementData.section.split('-').map(part => part.trim());

    const newSection = {
        call_number: sectionData[0],
        name: sectionData[1],
        bay: sectionPlacementData['id-bay'],
        start_column: sectionPlacementData.column1,
        end_column: sectionPlacementData.column2,
        start_line: `${sectionPlacementData["slider-line1"][0]}:${sectionPlacementData.line1}`,
        end_line: `${sectionPlacementData["slider-line2"][0]}:${sectionPlacementData.line2}`,
        subsections: []
    };

    let sectionExists = false;

    function updateSections(currentSections) {
        currentSections.forEach(section => {
            if (`${section.call_number} - ${section.name}`.trim() === sectionPlacementData.section) {
                if (section.bay === sectionPlacementData['id-bay'] || section.bay == null) {
                    // Même baie, mise à jour des valeurs
                    section.start_column = sectionPlacementData.column1;
                    section.end_column = sectionPlacementData.column2;
                    section.start_line = `${sectionPlacementData["slider-line1"][0]}:${sectionPlacementData.line1}`;
                    section.end_line = `${sectionPlacementData["slider-line2"][0]}:${sectionPlacementData.line2}`;
                    sectionExists = true;
                } else {
                    // Baie différente, ajouter une nouvelle section
                    currentSections.push(newSection);
                    sectionExists = true;
                }
            }

            if (section.subsections && section.subsections.length > 0) {
                updateSections(section.subsections);
            }
        });
    }

    updateSections(sections);

    if (!sectionExists) {
        // Parcourir la hiérarchie pour trouver le bon endroit pour ajouter la nouvelle section
        let currentLevel = sections;

        for (let i = 0; i < sectionData.length - 1; i++) {
            const part = sectionData[i];
            let matchingSection = currentLevel.find(section => `${section.call_number} - ${section.name}`.trim() === part);

            if (!matchingSection) {
                const newHierarchySection = {
                    call_number: sectionData[i],
                    name: sectionData[i + 1],
                    subsections: []
                };
                currentLevel.push(newHierarchySection);
                matchingSection = newHierarchySection;
            }
            currentLevel = matchingSection.subsections;
        }

        currentLevel.push(newSection);
    }

    return sections;
}

export function buildSectionHierarchy(sectionsData) {
    // Utiliser un Map pour stocker temporairement les sections par leur call_number
    const map = new Map();

    // Première passe pour créer les sections avec leurs détails
    sectionsData.forEach(sectionData => {
        const {
            call_number,
            name,
            bay,
            start_column,
            start_line,
            end_column,
            end_line
        } = sectionData;

        // Créer une nouvelle section avec les propriétés nécessaires
        const newSection = {
            call_number,
            name,
            subsections: []
        };

        // Ajouter les détails s'ils sont définis
        if (bay !== undefined && start_column !== undefined && start_line !== undefined &&
            end_column !== undefined && end_line !== undefined) {
            newSection.bay = bay;
            newSection.start_column = start_column;
            newSection.start_line = start_line;
            newSection.end_column = end_column;
            newSection.end_line = end_line;
        }

        // Ajouter la section au Map
        map.set(call_number, newSection);
    });

    // Fonction récursive pour ajuster les propriétés des sous-sections
    const adjustSubsections = (section) => {
        // Si la section a des sous-sections, traiter chaque sous-section
        section.subsections.forEach(subsection => {
            // Récursivement ajuster les sous-sections
            adjustSubsections(subsection);
        });

        // Si la section n'a pas de sous-sections, mettre les propriétés à null
        if (section.subsections.length === 0) {
            if (section.bay === undefined) section.bay = null;
            if (section.start_column === undefined) section.start_column = null;
            if (section.start_line === undefined) section.start_line = null;
            if (section.end_column === undefined) section.end_column = null;
            if (section.end_line === undefined) section.end_line = null;
        }
    };

    // Deuxième passe pour construire la hiérarchie
    const hierarchy = [];

    sectionsData.forEach(sectionData => {
        const { call_number } = sectionData;
        const parts = call_number.split('.');

        // Fonction pour trouver le parent basée sur votre méthode
        const findParent = (currentCallNumber) => {
            const parentParts = currentCallNumber.split('.');
            if (parentParts.length === 1) {
                return null; // Pas de parent pour les sections de premier niveau
            } else {
                let parent;
                let numbers = parentParts[1].split('');

                if (numbers.length > 1) {
                    parent = parentParts[0] + '.' + parentParts[1].slice(0, -1);
                } else {
                    parent = parentParts[0];
                }

                return parent;
            }
        };

        // Trouver le parent pour la section actuelle
        const parentCallNumber = findParent(call_number);

        // Ajouter la section à la hiérarchie correcte
        if (parentCallNumber) {
            const parentSection = map.get(parentCallNumber);
            if (parentSection) {
                const currentSection = map.get(call_number);
                if (currentSection) {
                    parentSection.subsections.push(currentSection);
                }
            }
        } else {
            hierarchy.push(map.get(call_number)); // Pour les sections de premier niveau
        }
    });

    // Appliquer la fonction adjustSubsections à la hiérarchie principale
    hierarchy.forEach(section => {
        adjustSubsections(section);
    });

    return hierarchy;
}
