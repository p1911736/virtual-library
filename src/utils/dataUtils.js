import { Vector3 } from 'three';
import { gpsCoordinatesTo3D, splitWithPattern } from './calculateUtils.js';
import bookshelfModels from '../json/models/bookshelfModels.json' assert { type: "json" };
import groundModels from '../json/models/groundModels.json' assert { type: "json" };
import itineraryModels from '../json/models/itineraryModels.json' assert { type: "json" };
import pillarModels from '../json/models/pillarModels.json' assert { type: "json" };
import stairModels from '../json/models/stairModels.json' assert { type: "json" };
import textModels from '../json/models/textModels.json' assert { type: "json" };
import wallModels from '../json/models/wallModels.json' assert { type: "json" };
import carrelModels from '../json/models/carrelModels.json' assert { type: "json" };
import windowModels from '../json/models/windowModels.json' assert { type: "json" };
import { CanvasTexture } from 'three';

export function getLevelTextData(floors)
{
    return {
        levelData: floors,
        model: 'A'
    }
}

export function getItineraryData()
{
    const itineraryData = {
        coordinates: [
            new Vector3(-4, 0,-15),
            new Vector3(-4, 0, -14),
            new Vector3(-3, 0, -14),
            new Vector3(-2, 0, -11),
        ],
        model: 'A',
        call_number_section: '009.121'
    };

    // setFloorHeight(itineraryData.itineraries);

    return itineraryData;
}

function setFloorHeight(data, floors) {

    data.forEach(element => {
        const floor = floors.find(floor => floor.level === element.level);
        const y = floor ? floor.position.y : 0;

        if (element.coordinates && Array.isArray(element.coordinates)) {
            element.coordinates.forEach(point => {
                point.y = y;
            });
        } else if (element.coordinates) {
            element.coordinates.y = y;
        }
    });
}

export function getBayAttributes(bayData, baysString) {

    const bayAttributes = [];
    
    const foundBay = bayData.find(bay => bay.id_bay_osm === baysString);

    if (foundBay) {
        bayAttributes.push({
            id_bay: foundBay.id_bay,
            models: foundBay.models,
            starting_position: foundBay.starting_position,
            end_position: foundBay.end_position
        });
    }

    return bayAttributes;
}

export function getModelAttributes(type, model) 
{
  let models;
  switch (type) {
    case 'bookshelf':
      models = bookshelfModels;
      break;
    case 'ground':
      models = groundModels;
      break;
    case 'itinerary':
      models = itineraryModels;
      break;
    case 'pillar':
      models = pillarModels;
      break;
    case 'stair':
      models = stairModels;
      break;
    case 'text':
      models = textModels;
      break;
    case 'wall':
      models = wallModels;
      break;
    case 'carrel':
      models = carrelModels;
      break;
    case 'window':
      models = windowModels;
      break;
    default:
      console.error(`Type "${type}" non pris en charge.`);
      return null;
  }

  const foundModel = models.find(item => item.model === model);
  
  if (foundModel) 
  {
    return foundModel;
  } 
  else 
  {
    console.error(`Le modèle ${model} pour le type ${type} n'a pas été trouvé.`);
    return null;
  }
}

export function accumulatedModelsWidth(models, range) 
{
    const splitModels = splitWithPattern(models, /([A-Z])(\d+)/);

    let startIndex = 0;
    let endIndex;

    if (Array.isArray(range)) {
        startIndex = range[0];
        endIndex = range[1] - 1;
    } else {
        endIndex = range !== undefined ? range : splitModels.length;
    }

    let accumulatedWidth = 0;

    for (let i = startIndex; i < endIndex; i++) {
        const model = splitModels[i][0];
        const modelAttributes = getModelAttributes("bookshelf", model);
        accumulatedWidth += modelAttributes.width;
    }

    return accumulatedWidth;
}



export function maxModelsHeight(models) 
{
    const splitModels = splitWithPattern(models, /([A-Z])(\d+)/);

    let maxHeight = 0;

    splitModels.forEach(model => {
        const modelIdentifier = model[0];
        const modelAttributes = getModelAttributes("bookshelf", modelIdentifier);
        if (modelAttributes.height > maxHeight) {
            maxHeight = modelAttributes.height;
        }
    });

    return maxHeight;
}

export function findSectionByCallNumber(sections, callNumber) {
    for (const section of sections) {
        
      if (section.call_number === callNumber) {
        return section;
      }
      if (section.subsections && section.subsections.length > 0) {
        const found = findSectionByCallNumber(section.subsections, callNumber);
        if (found) {
          return found;
        }
      }
    }
    return null;
}

export function createTextTexture(callNumber, name, width, height) {
    const canvas = document.createElement('canvas');
    canvas.width = width;
    canvas.height = height;
    const context = canvas.getContext('2d');

    context.font = `60px Arial`;
    context.fillStyle = 'white';
    context.textAlign = 'center';
    context.textBaseline = 'middle';

    const truncateText = (text, maxWidth) => {
        let truncatedText = text;
        const ellipsis = '...';
        if (context.measureText(text).width > maxWidth) {
            while (truncatedText.length > 0 && context.measureText(truncatedText + ellipsis).width > maxWidth) {
                truncatedText = truncatedText.slice(0, -1);
            }
            truncatedText += ellipsis;  // Ajoute les points de suspension uniquement si le texte est tronqué
        }
        return truncatedText;
    };

    const truncatedCallNumber = truncateText(callNumber, width - 30);
    const truncatedName = truncateText(name, width - 30);

    context.clearRect(0, 0, width, height);
    context.fillText(truncatedCallNumber, width / 2, height / 3);
    context.fillText(truncatedName, width / 2, 2 * height / 3);

    return new CanvasTexture(canvas);
}

// ----------------------

