import React from 'react';
import CameraSetup from '../Configuration/CameraSetup.jsx';
import { useGlobalContextLibrary } from '../../interfaces/VirtualLibrary/VirtualLibraryContext.jsx';

export default function LookAtBay({ sectionData }) 
{
    const { userHeight } = useGlobalContextLibrary();

    const lookAtPosition = sectionData.center;
    const numberColumns = sectionData.number;
    const bookshelfHeight = sectionData.height;

    const position = [lookAtPosition[0], userHeight, lookAtPosition[2] - bookshelfHeight * 1.5 - numberColumns * 0.25];

    return (
        <CameraSetup position={position} lookAtPosition={lookAtPosition} />
    );
};
