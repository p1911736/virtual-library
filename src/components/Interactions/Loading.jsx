import React from 'react';
import { PlaneGeometry, ShaderMaterial } from 'three';

export default function Loading()
{
    const planeGeometry = new PlaneGeometry(2, 2, 1, 1);

    const planeMaterial = new ShaderMaterial({ 
        transparent: true,
        uniforms: {
            uAlpha: { value: 1 }
        },
        vertexShader: `
            void main() {
                gl_Position = vec4(position, 0);
            }
        `,
        fragmentShader: `
            uniform float uAlpha;
            void main() {
                gl_FragColor = vec4(0.24, 0.24, 0.24, uAlpha);
            }
        `
    });

    return (
        <mesh
            geometry={planeGeometry}
            material={planeMaterial}
        />
    );
}