import { useMemo, useRef, useEffect, createRef, useState } from 'react';
import { getLevelTextData } from '../../utils/dataUtils';
import { Text } from '@react-three/drei';
import { useFrame, useThree } from '@react-three/fiber';
import { getModelAttributes } from '../../utils/dataUtils.js';

export default function LevelText()
{
    const { camera } = useThree();
    const textRefs = useRef([]);

    const levelTextData = useMemo(() => getLevelTextData(), []);
    const modelAttributes = useMemo(() => getModelAttributes("text", levelTextData.model), [levelTextData.model]);

    useEffect(() => {
        textRefs.current = new Array(levelTextData.levelData.length).fill().map((_, index) => textRefs.current[index] || createRef());
    }, [levelTextData.levelData.length]);
    
    // Met à jour la position des textes pour qu'ils regardent toujours la caméra
    useFrame(() => {
        if (camera) {
            textRefs.current.forEach((textRef) => {
                if (textRef.current) {
                    textRef.current.lookAt(camera.position);
                }
            });
        }
    });

    return (
        <>
            {levelTextData.levelData.map((text, index) => {
                return (
                    <group key={index}>
                        <Text 
                            ref={textRefs.current[index]} 
                            position={[text.position.x, text.position.y + 2, text.position.z]}
                            font={modelAttributes.font}
                            fontSize={modelAttributes.font_size} 
                            fontWeight={modelAttributes.font_weight}
                            color={modelAttributes.font_color}
                            textAlign={modelAttributes.text_align}
                            letterSpacing={modelAttributes.letter_spacing}
                            lineHeight={modelAttributes.line_height}
                            textDecoration={modelAttributes.text_decoration}
                            outlineWidth={modelAttributes.outline_width}
                            outlineColor={modelAttributes.outline_color}
                        >
                            {text.level} 
                            {"\n"}
                            {text.name}
                        </Text>
                    </group>
                );
            })}
        </>
    );
}