import React, { useState, useEffect, useMemo } from 'react';
import { useGlobalContextEdition } from '../../interfaces/EditionLibrary/SectionEdition/SectionEditionContext.jsx';
import '../../css/EditionLibrary/SectionEdition/sectionEdition.css';
import FormField from './FormField.jsx';
import axios from 'axios';
import ApiService from '../../api/ApiService.js';

export default function DynamicForm({ fields, selectData }) {

    const initializeFormState = useMemo(() => {
        const initialState = {};
        fields.forEach(field => {
            if (field.defaultValue !== undefined) {
                initialState[field.name] = field.defaultValue;
            } else if (field.type === 'checkbox') {
                initialState[field.name] = false;
            } else {
                initialState[field.name] = '';
            }
        });
        return initialState;
    }, [fields]);

    const [formState, setFormState] = useState(initializeFormState);
    const [selectedFloor, setSelectedFloor] = useState('');
    const { setCurrentBayData, currentSectionData, setCurrentSectionData } = useGlobalContextEdition();

    useEffect(() => {
        const defaultSectionData = {};
        fields.forEach(field => {
            if ([
                'section', 'column1', 'line1', 'slider-line1',
                'column2', 'line2', 'slider-line2',
            ].includes(field.name)) {
                defaultSectionData[field.name] = field.defaultValue || 0;
            }
        });
    }, [fields, setCurrentSectionData]);

    const handleChange = (e) => {
        const { name, type, checked, value } = e.target;
    
        const updatedFormState = {
            ...formState,
            [name]: type === 'checkbox' ? checked : value
        };
    
        if (name === 'id-bay') {
            if (value === '') {
                setCurrentBayData(null);
            } else {
                const selectedBay = selectData[0].find(bay => bay['id_bay_osm'] === value);
                setCurrentBayData(selectedBay);
            }
            // Initialise la section lorsque la travée change
            updatedFormState['section'] = '';
            setCurrentSectionData(prevSectionData => ({
                ...prevSectionData,
                selectedSection: null
            }));
        }

        if (name === 'floor') {
            setSelectedFloor(value);
        }
    
        setFormState(updatedFormState);
    
        switch (name) {
            case 'section':
                if (value === '') {
                    setCurrentSectionData(prevSectionData => ({
                        ...prevSectionData,
                        selectedSection: null
                    }));
                } else {
                    const selectedSection = selectData[1].find(section => section['name'] === value.split(' - ')[1]);
    
                    if (selectedSection) {
                        const updatedSectionData = {
                            ...formState,
                            column1: selectedSection.start_column,
                            line1: selectedSection.start_line.split(':')[1],
                            'slider-line1': [parseInt(selectedSection.start_line.split(':')[0])],
                            column2: selectedSection.end_column,
                            line2: selectedSection.end_line.split(':')[1],
                            'slider-line2': [parseInt(selectedSection.end_line.split(':')[0])]
                        };
                        setFormState(updatedSectionData);
                        setCurrentSectionData({ ...currentSectionData, selectedSection, ...updatedSectionData });
                    } else {
                        const defaultSectionData = {};
                        fields.forEach(field => {
                            defaultSectionData[field.name] = field.defaultValue || 0;
                        });
                        setCurrentSectionData(defaultSectionData);
                    }
                }
                break;
            default:
                setCurrentSectionData(prevSectionData => ({
                    ...prevSectionData,
                    [name]: type === 'checkbox' ? checked : value
                }));
                break;
        }
    };    

    const handleSliderChange = (name, values) => {
        setFormState({
            ...formState,
            [name]: values
        });

        setCurrentSectionData(prevSectionData => ({
            ...prevSectionData,
            [name]: values
        }));
    };


    const handleInputTypeChange = (field, type) => {
        setFormState({
            ...formState,
            [`${field}_type`]: type
        });
    };

    const getSelectData = (fieldName) => {
        switch (fieldName) {
            case 'id-bay':
                return selectData[0].filter(bay => bay['id_bay_osm'].startsWith(selectedFloor));
            case 'section':
                return selectData[1];
            case 'floor':
                return selectData[2];
            default:
                return [];
        }
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        const data = {
            ...formState
        };

        axios.post(`${ApiService.getAPI_URL()}/api/update-section-placement`, data)
          .then((response) => {
            console.log(response.data);
          })
          .catch((error) => {
            console.error('There was an error!', error);
          });
      };

      return (
        <form onSubmit={handleSubmit} className="form-container">
            <div className="form-row">
                {fields.slice(0, 3).map((field, index) => (
                    <div key={`${field.name}-${index}`} className="form-column">
                        <FormField
                            field={field}
                            formState={formState}
                            handleChange={handleChange}
                            handleSliderChange={handleSliderChange}
                            handleInputTypeChange={handleInputTypeChange}
                            getSelectData={getSelectData}
                        />
                    </div>
                ))}
            </div>
            <div className="form-columns">
                <div className="column">
                    {fields.slice(3, 7).map((field, index) => (
                        <FormField
                            key={`${field.name}-${index}`}
                            field={field}
                            formState={formState}
                            handleChange={handleChange}
                            handleSliderChange={handleSliderChange}
                            handleInputTypeChange={handleInputTypeChange}
                            getSelectData={getSelectData}
                        />
                    ))}
                </div>
                <div className="column">
                    {fields.slice(7, 11).map((field, index) => (
                        <FormField
                            key={`${field.name}-${index}`}
                            field={field}
                            formState={formState}
                            handleChange={handleChange}
                            handleSliderChange={handleSliderChange}
                            handleInputTypeChange={handleInputTypeChange}
                            getSelectData={getSelectData}
                        />
                    ))}
                </div>
            </div>
            <div className="form-footer">
                {fields.slice(11).map((field, index) => (
                    <FormField
                        key={`${field.name}-${index}`}
                        field={field}
                        formState={formState}
                        handleChange={handleChange}
                        handleSliderChange={handleSliderChange}
                        handleInputTypeChange={handleInputTypeChange}
                        getSelectData={getSelectData}
                    />
                ))}
            </div>
        </form>
    );
}
