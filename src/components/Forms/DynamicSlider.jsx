import React, { useEffect, useRef } from 'react';
import noUiSlider from 'nouislider';
import 'nouislider/dist/nouislider.css';

export default function DynamicSlider({ name, type, defaultValue, margin, step, handleSliderChange }) {
    const sliderRef = useRef(null);

    useEffect(() => {
        if (sliderRef.current) {
            if (sliderRef.current.noUiSlider) {
                sliderRef.current.noUiSlider.destroy();
            }
            
            const isDoubleSlider = type === 'slider-2';
            const connect = isDoubleSlider ? [false, true, false] : [true, false];
            
            noUiSlider.create(sliderRef.current, {
                start: defaultValue ? defaultValue : isDoubleSlider ? [25, 75] : 50,
                connect: connect,
                step: step ? step : 0.1,
                margin: margin ? margin : 5,
                tooltips: true,
                range: {
                    min: 0,
                    max: 100
                }
            });

            sliderRef.current.noUiSlider.on('update', (values) => {
                handleSliderChange(name, values);
            });
        }
    }, []);

    return (
        <div ref={sliderRef}></div>
    );
}
