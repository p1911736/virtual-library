import React from 'react';
import DynamicSlider from './DynamicSlider.jsx';
import '../../css/EditionLibrary/SectionEdition/sectionEdition.css';

export default function FormField ({field, formState, handleChange, handleSliderChange, handleInputTypeChange, getSelectData })
{
    const fieldClass = `${field.name}`;

    if (field.type === 'color-file') {
        const selectedType = formState[`${field.name}_type`] || 'color';
        return (
            <div className={fieldClass}>
                <label>{field.label}</label>
                <div>
                    <label>
                        <input
                            type="radio"
                            name={`${field.name}_type`}
                            value="color"
                            checked={selectedType === 'color'}
                            onChange={() => handleInputTypeChange(field.name, 'color')}
                        />
                        Couleur
                    </label>
                    <label>
                        <input
                            type="radio"
                            name={`${field.name}_type`}
                            value="file"
                            checked={selectedType === 'file'}
                            onChange={() => handleInputTypeChange(field.name, 'file')}
                        />
                        Fichier
                    </label>
                </div>
                {selectedType === 'color' ? (
                    <input
                        type="color"
                        name={`${field.name}_color`}
                        value={formState[`${field.name}_color`] || '#000000'}
                        onChange={handleChange}
                    />
                ) : (
                    <input
                        type="file"
                        name={field.name}
                        accept="image/*"
                        onChange={handleChange}
                    />
                )}
            </div>
        );
    } else if (field.type === 'slider-1' || field.type === 'slider-2') {
        return (
            <div className={fieldClass}>
                <label>{field.label}</label>
                <div className='slider-container'>
                    <DynamicSlider
                        name={field.name}
                        type={field.type}
                        defaultValue={formState[field.name]}
                        margin={field.margin}
                        step={field.step}
                        handleSliderChange={handleSliderChange}
                    />
                </div>
            </div>
        );
    } else if (field.type === 'select') {
        let options = [];
        if (field.name === 'section') {
            options = getSelectData(field.name).map(objet => ({
                value: objet.call_number + ' - ' + objet.name,
                label: objet.call_number + ' - ' + objet.name
            }));
        } else {
            options = getSelectData(field.name).map(objet => objet[field["select-type"]]);
            console.log(options);
        }
        return (
            <div className={fieldClass}>
                <label>{field.label}</label>
                <select
                    name={field.name}
                    value={formState[field.name] || ''}
                    onChange={handleChange}
                >
                    <option value=''>-</option>
                    {options && options.map(option => (
                        <option key={option.value || option} value={option.value || option}>
                            {option.label || option}
                        </option>
                    ))}
                </select>
            </div>
        );
    } else if (field.type === 'checkbox') {
        return (
            <div className={`checkbox-group ${fieldClass}`}>
                <input
                    type="checkbox"
                    name={field.name}
                    checked={formState[field.name] || false}
                    onChange={handleChange}
                />
                <label>{field.label}</label>
            </div>
        );
    } else if (field.type === 'button') {
        return (
            <button className={fieldClass} type="submit">
                {field.label}
            </button>
        );
    } else if (field.type === 'label') {
        if (field.name === 'start' || field.name === 'end') {
            return (
                <div className={`${fieldClass} ${field.name}`}>
                    <label>{field.label}</label>
                </div>
            );
        }
        else
        {
            return (
                <div className={fieldClass}>
                    <label>{field.label}</label>
                    <div>{formState[field.name]}</div>
                </div>
            );
        }
    } else {
        return (
            <div className={fieldClass}>
                <label>{field.label}</label>
                <input
                    type={field.type}
                    name={field.name}
                    value={formState[field.name] || ''}
                    onChange={handleChange}
                />
            </div>
        );
    }
};
