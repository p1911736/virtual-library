import React, { useState, useEffect } from 'react';

export default function DeviceDetector({ children }) 
{
    const [isMobile, setIsMobile] = useState(false);

    useEffect(() => {
        const detectDeviceType = () => {
            setIsMobile(/iPhone|iPad|iPod|Android/i.test(navigator.userAgent));
        };

        detectDeviceType();

        window.addEventListener('orientationchange', detectDeviceType);

        return () => {
            window.removeEventListener('orientationchange', detectDeviceType);
        };
    }, []);

    return children(isMobile);
};
