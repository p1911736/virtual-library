import { useFrame } from '@react-three/fiber';
import { useEffect, useRef } from 'react';

export default function CameraSetup({ position, fov, lookAtPosition, onUpdateComplete }) {
  const cameraUpdatedRef = useRef(false);

  useEffect(() => {
    // Réinitialise le flag d'état de mise à jour de la caméra
    cameraUpdatedRef.current = false;
  }, [position, lookAtPosition, fov]);

  useFrame((state) => {
    const cam = state.camera;

    if (position) cam.position.set(position[0], position[1], position[2]);

    if (lookAtPosition) {
      cam.lookAt(lookAtPosition[0], lookAtPosition[1], lookAtPosition[2]);
    } else {
      cam.lookAt(cam.position.x, cam.position.y, -cam.position.z);
    }

    if (fov) cam.fov = fov;

    cam.updateProjectionMatrix();

    // Marque la mise à jour de la caméra comme terminée
    if (!cameraUpdatedRef.current) {
      cameraUpdatedRef.current = true;
      if (onUpdateComplete) {
        onUpdateComplete();
      }
    }
  });

  return null;
}
