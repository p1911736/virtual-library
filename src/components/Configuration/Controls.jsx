import React, { useEffect } from 'react';
import { useThree } from '@react-three/fiber';

export function Controls() {
  const { camera, gl } = useThree();

  // Fonction pour déplacer la caméra vers un point spécifique
  const moveToPosition = (x, y, z) => {
    camera.position.set(x, camera.position.y, z);
    camera.lookAt(x, camera.position.y, z); // Garde la caméra à la même hauteur et la fait regarder vers le point
  };

  // Fonction pour gérer le zoom avec pinch
  const handlePinch = (e) => {
    const distance = Math.hypot(
      e.touches[0].clientX - e.touches[1].clientX,
      e.touches[0].clientY - e.touches[1].clientY
    );

    const zoomSpeed = 0.1; // Ajuster la vitesse de zoom
    const zoomFactor = distance > 0 ? distance / 100 : 0; // Ajuster le facteur de zoom

    camera.position.y -= zoomFactor * zoomSpeed; // Zoom négatif pour dézoomer
  };

  useEffect(() => {
    const handleClick = (e) => {
      // Récupérer les coordonnées du clic par rapport à l'élément WebGLRenderer
      const canvasBounds = gl.domElement.getBoundingClientRect();
      const mouseX = e.clientX - canvasBounds.left;
      const mouseY = e.clientY - canvasBounds.top;

      // Convertir les coordonnées de l'écran en coordonnées de l'espace 3D
      const x = (mouseX / gl.domElement.clientWidth) * 2 - 1;
      const y = -(mouseY / gl.domElement.clientHeight) * 2 + 1;

      // Créer un rayon à partir de la caméra vers le clic/tap
      const raycaster = new THREE.Raycaster();
      raycaster.setFromCamera({ x, y }, camera);

      // Récupérer les intersections avec les objets 3D (optionnel, dépend de votre scène)
      const intersects = raycaster.intersectObjects(scene.children, true);

      // Si une intersection est trouvée, déplacer la caméra vers ce point
      if (intersects.length > 0) {
        const { point } = intersects[0];
        moveToPosition(point.x, camera.position.y, point.z);
      }
    };

    const handlePinchStart = (e) => {
      window.addEventListener('touchmove', handlePinch);
    };

    const handlePinchEnd = () => {
      window.removeEventListener('touchmove', handlePinch);
    };

    window.addEventListener('click', handleClick);
    window.addEventListener('touchstart', handlePinchStart);
    window.addEventListener('touchend', handlePinchEnd);

    return () => {
      window.removeEventListener('click', handleClick);
      window.removeEventListener('touchstart', handlePinchStart);
      window.removeEventListener('touchend', handlePinchEnd);
      window.removeEventListener('touchmove', handlePinch);
    };
  }, []);

  return null;
}

export default Controls;
