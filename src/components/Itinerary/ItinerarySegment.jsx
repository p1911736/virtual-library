import { Vector3, Matrix4, Euler, BoxGeometry } from 'three';

export default function ItinerarySegment({ start, end, width, height }) 
{
    const distance = start.distanceTo(end);

    const position = start.clone().lerp(end, 0.5);
    position.y += height / 2;

    const rotationMatrix = new Matrix4();
    rotationMatrix.lookAt(start, end, new Vector3(0, 1, 0));
    const rotation = new Euler().setFromRotationMatrix(rotationMatrix);

    const segmentGeometry = new BoxGeometry(width, height, distance + width);
    segmentGeometry.rotateX(rotation.x);
    segmentGeometry.rotateY(rotation.y);
    segmentGeometry.rotateZ(rotation.z);
    segmentGeometry.translate(position.x, position.y, position.z);
    

    return segmentGeometry;
}
