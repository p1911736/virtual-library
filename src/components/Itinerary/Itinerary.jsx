import { useMemo, useState, useEffect } from 'react';
import { getBayAttributes, findSectionByCallNumber, getModelAttributes, accumulatedModelsWidth } from '../../utils/dataUtils.js';
import ItinerarySegment from './ItinerarySegment.jsx';
import { BufferGeometryUtils } from 'three/examples/jsm/Addons.js';
import { MeshPhysicalMaterial, Vector3, CylinderGeometry } from 'three';
import { calculateDirection } from '../../utils/calculateUtils.js';
import axios from 'axios';
import ApiService from '../../api/ApiService.js';
import { useGlobalContextLibrary } from '../../interfaces/VirtualLibrary/VirtualLibraryContext.jsx';

export default function Itinerary() {
  const [sectionsJson, setSectionsJson] = useState(null);
  const [bayData, setBayData] = useState(null);
  const [bookData, setBookData] = useState(null);
  const [geometries, setGeometries] = useState(null);
  const [material, setMaterial] = useState(null);
  const [cylinderGeometry, setCylinderGeometry] = useState(null);
  const [cylinderMaterial, setCylinderMaterial] = useState(null);
  const { libraryInfo, setIsSectionButtonVisible } = useGlobalContextLibrary();

  // Récupère les travées
  useEffect(() => {
    if (libraryInfo) {
      axios.get(`${ApiService.getAPI_URL()}/api/bays/${libraryInfo.id}`)
        .then(response => {
          setBayData(response.data);
        })
        .catch(error => {
          console.error('Error fetching bays JSON:', error);
        });
    }
  }, [libraryInfo]);

  // Récupère les sections 
  useEffect(() => {
    if (libraryInfo) {
      axios.get(`${ApiService.getAPI_URL()}/api/sections/${libraryInfo.id}`)
        .then(response => {
          setSectionsJson(response.data);
        })
        .catch(error => {
          console.error('Error fetching sections JSON:', error);
        });
    }
  }, [libraryInfo]);

  // Récupère les informations du livre recherché
  useEffect(() => {
    axios.post(`${ApiService.getAPI_URL()}/api/set-current-book`, [
      {
        "call_number": "009.121",
        "cover": "https://m.media-amazon.com/images/I/7145FhcPURL._SL1500_.jpg",
        "title": "Les réseaux informatiques par la pratique",
        "author": "Olivier Salvatori",
        "publication_date": "05/10/2018",
        "status": "Disponible"
      },
      {
        "coordinates": [
          { "x": -10, "y": 0, "z": -20 },
          { "x": -10, "y": 0, "z": -14 },
          { "x": -5, "y": 0, "z": -14 },
          { "x": -5, "y": 0, "z": -14 }
        ],
        "level": 0,
        "call_number_section": "009.121"
      }
    ])
      .then(response => {})
      .catch(error => {
        console.error('Error setting book JSON:', error);
      }
  )}, []);

  useEffect(() => {
    axios.get(`${ApiService.getAPI_URL()}/api/get-current-book`)
      .then(response => {
        setBookData(response.data);
      })
      .catch(error => {
        console.error('Error fetching book JSON:', error);
      }
  )}, []);
  
  useEffect(() => {
    if (!bookData || !bayData || !sectionsJson) return;
    
    // Récupère les données de l'itinéraire
    const coordinates = bookData[1].coordinates.map(coord => new Vector3(coord.x, coord.y, coord.z));
    const model = bookData[2].model;
    const call_number_section = bookData[0].call_number;

    // Récupère la section correspondant à la cote
    const section = findSectionByCallNumber(sectionsJson, call_number_section);
    
    // Récupère les attributs de la travée de la section
    const bayAttributes = bayData && section ? getBayAttributes(bayData, section.bay) : null;

    if (!bayAttributes || bayAttributes.length === 0 || !bayAttributes[0]) return;

    // Retire la dernière coordonnée
    const filteredCoordinates = coordinates.slice(0, -1);

    // Récupérer les attributs du modèle d'itinéraire
    const itineraryModelAttributes = getModelAttributes('itinerary', model);
    const { width: itineraryWidth, height: itineraryHeight, cylinder_radius, cylinder_height } = itineraryModelAttributes;

    const direction = calculateDirection(bayAttributes[0]);

    // Calculer les coordonnées finales
    const finalCoordinates = (() => {
      if (section && bayAttributes.length > 0) {
        const { start_column } = section;
        const { starting_position } = bayAttributes[0];
        const startingWidth = accumulatedModelsWidth(bayAttributes[0].models, start_column - 1);
        const columnWidth = accumulatedModelsWidth(bayAttributes[0].models, [start_column - 1, start_column + 1]);

        // Calculer la position en utilisant la largeur de la bay
        const startX = starting_position.x + (startingWidth + columnWidth / 2) * direction;

        // Ajouter la coordonnée finale
        return [...filteredCoordinates, new Vector3(startX, starting_position.y, starting_position.z + 0.65 * direction)];
      }
      return filteredCoordinates;
    })();

    // Construire la géométrie de l'itinéraire
    const { mergedGeometry, itineraryMaterial } = (() => {
      const geometries = [];
      const material = new MeshPhysicalMaterial({ color: itineraryModelAttributes.furniture_material });
      
      finalCoordinates.forEach((coordinate, coordinateIndex, coordinates) => {
        if (coordinateIndex < coordinates.length - 1) {
          const start = coordinate;
          const end = coordinates[coordinateIndex + 1];
          const segmentGeometry = ItinerarySegment({ start, end, width: itineraryWidth, height: itineraryHeight });
          geometries.push(segmentGeometry);
        }
      });

      const mergedGeometry = BufferGeometryUtils.mergeGeometries(geometries);
      return { mergedGeometry, itineraryMaterial: material };
    })();

    setGeometries(mergedGeometry);
    setMaterial(itineraryMaterial);

    // Créer la géométrie et le matériau du cylindre
    const lastCoordinate = finalCoordinates[finalCoordinates.length - 1];
    const geometry = new CylinderGeometry(cylinder_radius, cylinder_radius, cylinder_height);
    geometry.translate(new Vector3(lastCoordinate.x, lastCoordinate.y + cylinder_height / 2, lastCoordinate.z));

    setCylinderGeometry(geometry);
    setCylinderMaterial(new MeshPhysicalMaterial({ color: '#F94040' }));
    
    setIsSectionButtonVisible(true);
  }, [bookData, bayData, sectionsJson]);

  return (
    <>
      {geometries && material && (
        <mesh geometry={geometries} material={material} />
      )}
      {cylinderGeometry && cylinderMaterial && (
        <mesh geometry={cylinderGeometry} material={cylinderMaterial} />
      )}
    </>
  );
}
