import React from 'react';
import { useGlobalContextLibrary } from '../../interfaces/VirtualLibrary/VirtualLibraryContext.jsx';
import '../../css/VirtualLibrary/book-button-interface.css';

export default function BookButtonInterface() {
    const { isSectionButtonVisible, setIsSectionButtonVisible } = useGlobalContextLibrary();

    const toggleInterface = () => {
        setIsSectionButtonVisible(!isSectionButtonVisible);
    };

    return (
        <div className="book-button-container">
            <button onClick={toggleInterface}>
                {isSectionButtonVisible ? 'Sortir de la section' : 'Voir la section'}
            </button>
        </div>
    );
}
