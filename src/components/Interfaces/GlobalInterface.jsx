import React, { useState, useEffect } from 'react';
import { FaArrowLeft, FaStreetView, FaLayerGroup, FaBuilding } from 'react-icons/fa';
import '../../css/VirtualLibrary/global-interface.css';
import { useGlobalContextLibrary } from '../../interfaces/VirtualLibrary/VirtualLibraryContext.jsx';
import axios from 'axios';
import ApiService from '../../api/ApiService.js';
import CameraSetup from '../Configuration/CameraSetup.jsx';
import { Vector3 } from 'three';

export default function GlobalInterface({ libraryId, onFloorClick }) {
  const [isEyeIcon, setIsEyeIcon] = useState(true);
  const [floorsVisible, setFloorsVisible] = useState(true);
  const { bookInterfaceVisible, libraryInfo } = useGlobalContextLibrary();
  const [floorCount, setFloorCount] = useState(0);
  const [floors, setFloors] = useState([]);

  const toggleViewModeIcon = () => {
    setIsEyeIcon(!isEyeIcon);
  };

  const toggleFloorsIcon = () => {
    setFloorsVisible(!floorsVisible);
  };

  useEffect(() => {
    if (libraryId)
    {
      axios.get(`${ApiService.getAPI_URL()}/api/floors/${libraryId}`)
        .then(response => {
          setFloors(response.data);
          setFloorCount(response.data.length);
        })
        .catch(error => {
          console.error('Error fetching floors JSON:', error);
        });
    }
  }, [libraryId]);

  const reversedFloors = Array.from({ length: floorCount }, (_, index) => floorCount - index - 1);

  const handleFloorClick = (floorIndex) => {
    const floor = floors.find(floor => floor.level === floorIndex);
    if (floor) {
      const position = isEyeIcon ? floor['3d_position'] : floor.global_position;
      onFloorClick([position.x, position.y, position.z - 2], [position.x, position.y, position.z], 45);
    }
  };

  return (
    <>
      {!bookInterfaceVisible && (
        <div className="global-interface">
          {/* Header */}
          <div className="global-interface-header">
            <div className="back-icon">
              <FaArrowLeft />
            </div>
            <div className="library-name">
              {libraryInfo.name}
            </div>
          </div>
          
          {/* Icônes mode de vue */}
          <div className="view-mode-icon" onClick={toggleViewModeIcon}>
            {isEyeIcon ? <FaStreetView /> : <FaLayerGroup />}
          </div>

          {/* Étages */}
          <>
            {/* Icône bâtiment */}
            <div className="floors-icon" onClick={toggleFloorsIcon}>
              <FaBuilding />
            </div>

            {/* Icônes étages */}
            {floorsVisible && (
              <div className="floors">
                {reversedFloors.map((floorIndex, index) => (
                  <div key={index} className="floor" onClick={() => handleFloorClick(floorIndex)}>
                    {floorIndex}
                  </div>
                ))}
              </div>
            )}
          </>
        </div>
      )}
    </>
  );
}
