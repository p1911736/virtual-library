import React, { useMemo, useEffect } from 'react';
import '../../css/VirtualLibrary/book-interface.css';
import { useGlobalContextLibrary } from '../../interfaces/VirtualLibrary/VirtualLibraryContext.jsx'

// Remplacez ces données brutes du livre en utilisant les données du livre recherché
const bookData = {
    title: 'Le Seigneur des Anneaux',
    cover: 'https://images-na.ssl-images-amazon.com/images/I/51Zymoq7UnL._SX331_BO1,204,203,200_.jpg',
    author: 'J.R.R. Tolkien',
    publication_date: '29/07/1954',
    call_number: '901.121',
    status: 'Disponible',
};

export default function BookInterface() {
    const { bookInterfaceVisible, setBookInterfaceVisible } = useGlobalContextLibrary();
    setBookInterfaceVisible(true);
    const onClose = () => {
        setBookInterfaceVisible(false);
    };

    return (
        bookInterfaceVisible && (
            <div className="book-interface">
                <div className="interface-container" onClick={onClose}>
                    <div className="header-container">

                        {/* Titre */}
                        <div className="book-title">
                            {bookData.title}
                        </div>
                    </div>

                    {/* Contenu */}
                    <div className="content-container">
                        {/* Couverture */}
                        <div className="book-cover">
                            <img src={`${bookData.cover}`} alt="Couverture du livre" />
                        </div>

                        {/* Détails */}
                        <div className="book-details">
                            {/* Auteur */}
                            <div className="book-author">
                                <div className="author-title">
                                    Auteur :
                                </div>
                                <div className="author-name">
                                    {bookData.author}
                                </div>
                            </div>

                            {/* Date de publication */}
                            <div className="book-publication-date">
                                <div className="publication-title">
                                    Date de publication :
                                </div>
                                <div className="publication-date">
                                    {bookData.publication_date}
                                </div>
                            </div>

                            {/* Cote */}
                            <div className="book-call-number">
                                <div className="call-number-title">
                                    Cote :
                                </div>
                                <div className="call-number">
                                    {bookData.call_number}
                                </div>
                            </div>

                            {/* Statut */}
                            <div className="book-status">
                                <div className="status-title">
                                    Statut :
                                </div>
                                <div className="status">
                                    {bookData.status}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    );
}
