import React, { useState, useEffect } from 'react';
import { BoxGeometry, CylinderGeometry, MeshPhysicalMaterial } from 'three';
import { BufferGeometryUtils } from 'three/examples/jsm/Addons.js';
import axios from 'axios';
import ApiService from '../../api/ApiService.js';
import { calculateCentroid } from '../../utils/calculateUtils.js';
import { getModelAttributes } from '../../utils/dataUtils.js';
import { useGlobalContextLibrary } from '../../interfaces/VirtualLibrary/VirtualLibraryContext.jsx';

export default function Pillars() {
  const [pillarData, setPillarData] = useState([]);
  const [geometries, setGeometries] = useState({});
  const [materials, setMaterials] = useState({});
  const { libraryInfo } = useGlobalContextLibrary();

  // Charger les données des piliers
  useEffect(() => {
    if (libraryInfo.id) {
      axios.get(`${ApiService.getAPI_URL()}/api/pillars/${libraryInfo.id}`)
        .then(response => {
          setPillarData(response.data);
        })
        .catch(error => {
          console.error('Error fetching pillars JSON:', error);
        });
    }
  }, [libraryInfo]);

  // Conversion des modèles en géométries et matériaux
  useEffect(() => {
    if (pillarData.length > 0) {
      const allGeometries = {};
      const newMaterials = {};

      pillarData.forEach((pillar) => {
        const splitModels = Array.from(pillar.model.matchAll(/([A-Z])(\d*(?:\.\d+)?)/g), match => [match[1], match[2] || ""]);
                    
        const modelLetter = splitModels[0][0];
        const pillarModelData = getModelAttributes('pillar', modelLetter);

        const { height, furniture_material } = pillarModelData;

        let geometry;

        if (modelLetter === 'A') {
          const bottom_size = parseFloat(splitModels[1][1]);
          const side_size = parseFloat(splitModels[2][1]);
          geometry = new BoxGeometry(bottom_size, height, side_size);
        } else if (modelLetter === 'B') {
          const radius = parseFloat(splitModels[1][1]);
          geometry = new CylinderGeometry(radius, radius, height, 32);
        }

        if (!allGeometries[furniture_material]) {
          allGeometries[furniture_material] = [];
          newMaterials[furniture_material] = new MeshPhysicalMaterial({ color: furniture_material });
        }

        const position = calculateCentroid(pillar.coordinates);
        position.y = pillar.height / 2;

        geometry.translate(position.x, position.y, position.z);

        allGeometries[furniture_material].push(geometry);
      });

      const mergedGeometries = Object.keys(allGeometries).reduce((acc, materialKey) => {
        acc[materialKey] = BufferGeometryUtils.mergeGeometries(allGeometries[materialKey]);
        return acc;
      }, {});

      setGeometries(mergedGeometries);
      setMaterials(newMaterials);
    }
  }, [pillarData]);

  // Affichage des meshes lorsque les géométries sont prêtes
  return (
    <>
      {Object.keys(geometries).map((materialKey) => (
        <mesh key={materialKey} geometry={geometries[materialKey]} material={materials[materialKey]} />
      ))}
    </>
  );
}
