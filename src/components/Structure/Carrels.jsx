import React, { useState, useEffect } from 'react';
import { BoxGeometry, MeshPhysicalMaterial } from 'three';
import { BufferGeometryUtils } from 'three/examples/jsm/Addons.js';
import { getModelAttributes } from '../../utils/dataUtils.js';
import { calculateAttributesBetweenPoints, convertCoordinatesToVector3 } from '../../utils/calculateUtils.js';
import { useGlobalContextLibrary } from '../../interfaces/VirtualLibrary/VirtualLibraryContext.jsx';
import axios from 'axios';
import ApiService from '../../api/ApiService.js';
import { epsilon } from '../../utils/constants.js';

export default function Carrels() {
  const [carrelData, setCarrelData] = useState([]);
  const [geometries, setGeometries] = useState({});
  const [materials, setMaterials] = useState({});
  const { libraryInfo } = useGlobalContextLibrary();

  // Charger les données des carrels
  useEffect(() => {
    if (libraryInfo.id) {
      axios.get(`${ApiService.getAPI_URL()}/api/carrels/${libraryInfo.id}`)
        .then(response => {
          setCarrelData(response.data);
        })
        .catch(error => {
          console.error('Error fetching carrels JSON:', error);
        });
    }
  }, [libraryInfo]);

  // Calculer les géométries et matériaux lorsque carrelData change
  useEffect(() => {
    if (carrelData.length > 0) {
      const convertedData = convertCoordinatesToVector3(carrelData);

      const allGeometries = {};
      const newMaterials = {};

      convertedData.forEach((carrel) => {
        const { model, coordinates } = carrel;
        const [modelType, wallString, glassString] = model.split('-');
        const wallIndices = wallString ? wallString.substring(1).split(';').map(Number) : [];
        const glassIndices = glassString ? glassString.substring(1).split(';').map(Number) : [];

        wallIndices.forEach((index) => {
          const pointA = coordinates[(index - 1) % coordinates.length];
          const pointB = coordinates[index % coordinates.length];

          const modelAttributes = getModelAttributes('carrel', modelType);
          const { thickness, height, furniture_material, glass_material } = modelAttributes;

          const { length, position, rotation } = calculateAttributesBetweenPoints(pointA, pointB, height);

          const wallMaterialColor = furniture_material;
          const wallMaterial = new MeshPhysicalMaterial({ color: wallMaterialColor });

          const glassMaterialColor = glass_material;
          const glassMaterial = new MeshPhysicalMaterial({ color: glassMaterialColor, transparent: true, opacity: 0.6, metalness: 0.3 });

          const glassWall = glassIndices.includes(index);
          const materialKey = glassWall ? glassMaterialColor : wallMaterialColor;
          const material = glassWall ? glassMaterial : wallMaterial;

          if (!allGeometries[materialKey]) {
            allGeometries[materialKey] = [];
            newMaterials[materialKey] = material;
          }

          const boxGeometry = new BoxGeometry(glassWall ? thickness - 2 * epsilon : thickness, glassWall ? height - 2 * epsilon : height - epsilon, length + thickness - epsilon);
          boxGeometry.rotateY(rotation.y);
          boxGeometry.translate(position.x, position.y, position.z);

          allGeometries[materialKey].push(boxGeometry);
        });
      });

      const mergedGeometries = Object.keys(allGeometries).reduce((acc, materialKey) => {
        acc[materialKey] = BufferGeometryUtils.mergeGeometries(allGeometries[materialKey]);
        return acc;
      }, {});

      setGeometries(mergedGeometries);
      setMaterials(newMaterials);
    }
  }, [carrelData]);

  // Affichage des meshes lorsque les géométries sont prêtes
  return (
    <>
      {Object.keys(geometries).map((materialKey) => (
        <mesh key={materialKey} geometry={geometries[materialKey]} material={materials[materialKey]} />
      ))}
    </>
  );
}
