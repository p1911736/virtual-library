import React, { useState, useEffect, useMemo } from 'react';
import { PlaneGeometry, DoubleSide, MeshPhysicalMaterial, Vector3, RepeatWrapping } from 'three';
import { TextureLoader } from 'three/src/loaders/TextureLoader';
import { BufferGeometryUtils } from 'three/examples/jsm/Addons.js';
import { getModelAttributes } from '../../utils/dataUtils.js';
import { calculateGroundSize } from '../../utils/calculateUtils.js';
import { useGlobalContextLibrary } from '../../interfaces/VirtualLibrary/VirtualLibraryContext.jsx';
import axios from 'axios';
import ApiService from '../../api/ApiService.js';

export default function Ground() {
  const [groundData, setGroundData] = useState(null);
  const [textures, setTextures] = useState({});
  const [geometries, setGeometries] = useState({});
  const [materials, setMaterials] = useState({});
  const { libraryInfo } = useGlobalContextLibrary();

  // Charger les données du sol
  useEffect(() => {
    if (libraryInfo.id) {
      axios.get(`${ApiService.getAPI_URL()}/api/grounds/${libraryInfo.id}`)
        .then(response => {
          setGroundData(response.data);
        })
        .catch(error => {
          console.error('Error fetching floors JSON:', error);
        });
    }
  }, [libraryInfo]);

  // Charger les textures lorsque groundData change
  const textureLoader = useMemo(() => new TextureLoader(), []);
  useEffect(() => {
    if (groundData) {
      const uniqueMaterials = new Set(groundData.map(ground => {
        return getModelAttributes('ground', ground.model).furniture_material;
      }));

      const textureMap = {};
      uniqueMaterials.forEach(material => {
        const texture = textureLoader.load(material);
        texture.wrapS = RepeatWrapping;
        texture.wrapT = RepeatWrapping;
        textureMap[material] = texture;
      });

      setTextures(textureMap);
    }
  }, [groundData, textureLoader]);

  // Calculer les géométries et les matériaux lorsque groundData ou textures changent
  useEffect(() => {
    if (groundData && Object.keys(textures).length > 0) {
      const allGeometries = {};
      const newMaterials = {};

      groundData.forEach((ground) => {
        const groundSize = calculateGroundSize(ground.coordinates);
        const modelAttributes = getModelAttributes('ground', ground.model);
        const { furniture_material } = modelAttributes;

        const texture = textures[furniture_material];
        if (texture) {
          texture.repeat.set(groundSize.width, groundSize.depth);
        }

        const planeGeometry = new PlaneGeometry(groundSize.width, groundSize.depth);
        planeGeometry.rotateX(-Math.PI / 2);
        planeGeometry.translate(new Vector3(groundSize.position[0], groundSize.position[1], groundSize.position[2]));

        if (!allGeometries[furniture_material]) {
          allGeometries[furniture_material] = [];
          newMaterials[furniture_material] = new MeshPhysicalMaterial({ map: texture, side: DoubleSide });
        }

        allGeometries[furniture_material].push(planeGeometry);
      });

      const mergedGeometries = Object.keys(allGeometries).reduce((acc, materialKey) => {
        acc[materialKey] = BufferGeometryUtils.mergeGeometries(allGeometries[materialKey]);
        return acc;
      }, {});

      setGeometries(mergedGeometries);
      setMaterials(newMaterials);
    }
  }, [groundData, textures]);

  // Affichage des meshes lorsque les géométries sont prêtes
  return (
    <>
      {Object.keys(geometries).map((materialKey) => (
        <mesh key={materialKey} geometry={geometries[materialKey]} material={materials[materialKey]} />
      ))}
    </>
  );
}
