import { Vector3 } from 'three';
import { useMemo } from 'react';
import { getModelAttributes } from '../../../../utils/dataUtils.js';
import { calculateDirection, splitWithPattern } from '../../../../utils/calculateUtils.js';
import { BufferGeometryUtils } from 'three/examples/jsm/Addons.js';
import Bookshelf from './Bookshelf.jsx';

export default function Bay({ bay }) {
  const { geometries, materials } = useMemo(() => {
    const mergedGeometries = new Map();
    const materials = new Map();

    const { models, starting_position } = bay;
    let currentXPosition = starting_position.x;
    const direction = calculateDirection(bay);
    const splitModels = splitWithPattern(models, /([A-Z])(\d+)/);

    splitModels.forEach((model, modelIndex) => {
      const modelIdentifier = model[0];
      const modelAttributes = getModelAttributes("bookshelf", modelIdentifier);

      const position = new Vector3(
        currentXPosition,
        starting_position.y,
        starting_position.z
      );

      currentXPosition += modelAttributes.width * direction;

      const leftDividerVisible = direction === 1 ? (modelIndex === 0) : (modelIndex === splitModels.length - 1);
      const rightDividerVisible = direction === 1 ? (modelIndex === splitModels.length - 1) : (modelIndex === 0);

      const dividersVisible = { leftDividerVisible, rightDividerVisible };

      const { geometry, material } = Bookshelf({ model: modelAttributes, boardsNumber: model[1], position, dividersVisible, direction });

      if (!mergedGeometries.has(modelIdentifier)) {
        mergedGeometries.set(modelIdentifier, []);
        materials.set(modelIdentifier, material);
      }

      mergedGeometries.get(modelIdentifier).push(geometry);
    });

    return { geometries: mergedGeometries, materials };
  }, [bay]);

  const mergedGeometries = useMemo(() => {
    const merged = {};
    geometries.forEach((geometryArray, modelIdentifier) => {
      merged[modelIdentifier] = BufferGeometryUtils.mergeGeometries(geometryArray);
    });
    return merged;
  }, [geometries]);

  return (
    <>
      {Object.keys(mergedGeometries).map((modelIdentifier) => (
        <mesh key={modelIdentifier} geometry={mergedGeometries[modelIdentifier]} material={materials.get(modelIdentifier)} />
      ))}
    </>
  );
}
