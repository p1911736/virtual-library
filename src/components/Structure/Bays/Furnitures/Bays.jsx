import React, { useState, useEffect, useMemo } from 'react';
import axios from 'axios';
import ApiService from '../../../../api/ApiService.js';
import { useGlobalContextLibrary } from '../../../../interfaces/VirtualLibrary/VirtualLibraryContext.jsx';
import Bay from './Bay.jsx';

export default function Bays() {
  const [bayData, setBayData] = useState([]);
  const { libraryInfo } = useGlobalContextLibrary();

  // Charger les données des bays
  useEffect(() => {
    if (libraryInfo.id) {
      axios.get(`${ApiService.getAPI_URL()}/api/bays/${libraryInfo.id}`)
        .then(response => {
          setBayData(response.data);
        })
        .catch(error => {
          console.error('Error fetching bays JSON:', error);
        });
    }
  }, [libraryInfo]);

  return (
    <>
      {bayData.map((bay, index) => (
        <Bay key={index} bay={bay} />
      ))}
    </>
  );
}
