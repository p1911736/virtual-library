import { BoxGeometry, SphereGeometry, MeshPhysicalMaterial } from 'three';
import { BufferGeometryUtils } from 'three/examples/jsm/Addons.js';
import { epsilon } from '../../../../utils/constants';

export default function Bookshelf({ model, boardsNumber, position, dividersVisible, direction }) {
  const { width, height, depth, lower_plinth_height, middle_boards_height, upper_plinth_height, central_dividers_width, side_dividers_width, backboard_depth, wheel_diameter, furniture_material, wheel_material } = model;
  const hasWheelsBool = wheel_diameter !== 0;

  const newPosition = { ...position };
  newPosition.x += width / 2 * direction;
  newPosition.y += height / 2 + lower_plinth_height - epsilon / 2 + (hasWheelsBool ? wheel_diameter : 0);

  const geometries = [];

  // Plinthe du haut
  const topBoard = new BoxGeometry(width, upper_plinth_height, depth);
  topBoard.translate(newPosition.x, newPosition.y + height / 2, newPosition.z);
  geometries.push(topBoard);

  // Planches horizontales
  for (let i = 0; i < boardsNumber; i++) {
    const posY = newPosition.y - (height / 2) + (height / boardsNumber) * i;
    const board = new BoxGeometry(width, middle_boards_height, depth);
    board.translate(newPosition.x, posY, newPosition.z);
    geometries.push(board);
  }

  // Planche verticale à gauche
  const leftBoard = new BoxGeometry(dividersVisible.leftDividerVisible ? side_dividers_width : central_dividers_width, height, depth);
  leftBoard.translate(newPosition.x - width / 2 + (dividersVisible.leftDividerVisible ? side_dividers_width : central_dividers_width) / 2, newPosition.y, newPosition.z);
  geometries.push(leftBoard);

  // Planche verticale à droite
  const rightBoard = new BoxGeometry(dividersVisible.rightDividerVisible ? side_dividers_width : central_dividers_width, height, depth);
  rightBoard.translate(newPosition.x + width / 2 - (dividersVisible.rightDividerVisible ? side_dividers_width : central_dividers_width) / 2, newPosition.y, newPosition.z);
  geometries.push(rightBoard);

  // Planche derrière
  if (backboard_depth > 0) {
    const backBoard = new BoxGeometry(width, height + lower_plinth_height + 0.025, backboard_depth);
    backBoard.translate(newPosition.x, newPosition.y - lower_plinth_height / 2 + 0.0125, newPosition.z - (depth / 2) * direction);
    geometries.push(backBoard);
  }

  // Plinthe du bas
  const plinth = new BoxGeometry(width, lower_plinth_height, depth);
  plinth.translate(newPosition.x, newPosition.y - height / 2 - lower_plinth_height / 2, newPosition.z);
  geometries.push(plinth);

  // Roulettes
  if (hasWheelsBool) {
    const wheelPositions = [
      { x: -width / 2 + 0.05, z: -depth / 2 + 0.05 },
      { x: width / 2 - 0.05, z: -depth / 2 + 0.05 },
      { x: -width / 2 + 0.05, z: depth / 2 - 0.05 },
      { x: width / 2 - 0.05, z: depth / 2 - 0.05 }
    ];
    wheelPositions.forEach(pos => {
      const wheel = new SphereGeometry(wheel_diameter / 2);
      wheel.translate(newPosition.x + pos.x, newPosition.y - height / 2 - lower_plinth_height / 2 - wheel_diameter, newPosition.z + pos.z);
      geometries.push(wheel);
    });
  }

  const mergedGeometry = BufferGeometryUtils.mergeGeometries(geometries);
  const material = new MeshPhysicalMaterial({ color: furniture_material });

  return { geometry: mergedGeometry, material };
}