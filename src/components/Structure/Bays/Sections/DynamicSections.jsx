import { useState, useEffect } from 'react';
import { extractSectionsAtLevel, fillSectionInfo } from '../../../../utils/jsonUtils.js'; 
import Sections from './Sections.jsx';
import { useFrame } from '@react-three/fiber';
import { calculateCameraBayDistance, generateLightColor } from '../../../../utils/calculateUtils.js';
import axios from 'axios';
import ApiService from '../../../../api/ApiService.js';
import { useGlobalContextLibrary } from '../../../../interfaces/VirtualLibrary/VirtualLibraryContext.jsx';

export default function DynamicSections() {
    const [currentLevel, setCurrentLevel] = useState(0);
    const [visibleSections, setVisibleSections] = useState(true);
    const [sectionsJson, setSectionsJson] = useState(null);
    const [sectionInfo, setSectionInfo] = useState([]);
    const { libraryInfo } = useGlobalContextLibrary();
    const [bayData, setBayData] = useState(null);

    useEffect(() => {
        if (libraryInfo.id) {
            axios.get(`${ApiService.getAPI_URL()}/api/sections/${libraryInfo.id}`)
                .then(response => {
                    setSectionsJson(response.data);
                })
                .catch(error => {
                    console.error('Error fetching sections JSON:', error);
                });
        }
    }, [libraryInfo]);

    useEffect(() => {
        if (libraryInfo.id) {
            axios.get(`${ApiService.getAPI_URL()}/api/bays/${libraryInfo.id}`)
                .then(response => {
                    setBayData(response.data);
                })
                .catch(error => {
                    console.error('Error fetching bays JSON:', error);
                });
        }
    }, [libraryInfo]);

    useEffect(() => {
        if (sectionsJson) {
            // Récupère les sections du niveau demandé
            const sectionDataAtLevel = extractSectionsAtLevel(sectionsJson, currentLevel);

            // Attribue une couleur aléatoire à chaque section
            const sectionColored = sectionDataAtLevel.map(section => {
                section.color = generateLightColor();
                return section;
            });

            // Remplit les informations des sections
            const coloredSections = fillSectionInfo(sectionColored);

            // Filtre les sections qui ont des attributs valides
            const filteredSections = coloredSections.filter(section =>
                section.start_column !== null &&
                section.start_line !== null &&
                section.end_column !== null &&
                section.end_line !== null
            );

            setSectionInfo(filteredSections);
        }
    }, [sectionsJson, currentLevel]);

    useFrame((state) => {
        if (bayData && sectionInfo.length > 0) {
            const cameraPosition = state.camera.position;
    
            const maxDistance = 23;
            let minDistance = Infinity;
    
            for (const section of sectionInfo) {
                // Vérifier que section.bay existe dans bayData
                const bayAttributes = bayData.find(bay => bay.id_travee_osm_depart === section.bay);
    
                // Vérifier si bayAttributes est défini et contient au moins un élément
                if (bayAttributes && bayAttributes.length > 0 && bayAttributes[0]) {
                    const { starting_position } = bayAttributes[0];
                    
                    // Continuez avec vos calculs en utilisant `starting_position`
                    const distance = calculateCameraBayDistance(cameraPosition, bayData, section.bay);
                    if (distance < minDistance) {
                        minDistance = distance;
                    }
                }
            }
    
            let newLevel = Math.max(0, Math.min(3, Math.floor((maxDistance - minDistance) / 4)));
    
            // if (minDistance >= 50) {
            //     setVisibleSections(false);
            // } else {
            //     setCurrentLevel(newLevel);
            //     setVisibleSections(true);
            // }
            setCurrentLevel(newLevel);
            setVisibleSections(true);
        }
    });

    // Affiche les sections filtrées
    return visibleSections && <Sections sections={sectionInfo} isPreview={false} libraryId={libraryInfo.id}/>;
}
