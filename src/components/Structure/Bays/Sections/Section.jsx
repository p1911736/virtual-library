import React, { useEffect, useState } from 'react';
import { BufferGeometry, BoxGeometry, MeshPhysicalMaterial, Matrix4, EdgesGeometry, LineBasicMaterial, MeshBasicMaterial, FrontSide } from 'three';
import { BufferGeometryUtils } from 'three/examples/jsm/Addons.js';
import { getBayAttributes, getModelAttributes, accumulatedModelsWidth, createTextTexture } from '../../../../utils/dataUtils.js';
import { splitWithPattern, calculateDirection, splitRange, darkenHexColor } from '../../../../utils/calculateUtils.js';
import { useGlobalContextLibrary } from '../../../../interfaces/VirtualLibrary/VirtualLibraryContext.jsx';
import { epsilon } from '../../../../utils/constants.js';
import axios from 'axios';
import ApiService from '../../../../api/ApiService.js';
import { bookParameters } from '../../../../utils/constants.js';

export default function Section({ section, isPreview = false, libraryId }) {
    const { setSectionInformation } = useGlobalContextLibrary();
    const [bayData, setBayData] = useState(null);
    const [sectionsMeshes, setSectionsMeshes] = useState(null);

    const bookWidth = bookParameters.width;
    const bookSpacing = bookParameters.spacing;

    useEffect(() => {
        if (libraryId) {
            axios.get(`${ApiService.getAPI_URL()}/api/bays/${libraryId}`)
                .then(response => {
                    setBayData(response.data);
                })
                .catch(error => {
                    console.error('Error fetching bays JSON:', error);
                });
        }
    }, [libraryId]);

    useEffect(() => {
        if (!bayData) return;

        const { bay, start_column, end_column } = section;
        const [startLinePosition, startLine] = splitRange(section.start_line);
        const [endLinePosition, endLine] = splitRange(section.end_line);
        const bayAttributes = isPreview ? bay : getBayAttributes(bayData, bay)[0];

        if (!bayAttributes) {
            return;
        }

        const sectionMaterial = new MeshPhysicalMaterial({ color: section.color, transparent: true, opacity: 0.5 });
        const bookMaterial = new MeshPhysicalMaterial({ color: section.color });
        const edgesMaterial = new LineBasicMaterial({ color: darkenHexColor(section.color, 0.2) });

        let bookGeometries = [];
        let edgesGeometries = [];
        let textMaterials = [];

        const { starting_position } = bayAttributes;
        const splitModels = splitWithPattern(bayAttributes.models, /([A-Z])(\d+)/);

        const sameColumnAndLine = start_column === end_column && startLine === endLine;

        for (let column = start_column ? start_column : 1; column <= end_column; column++) {
            const bayColumn = column - 1;
            const bayModels = splitModels[bayColumn];

            if (bayModels) {
                const model = bayModels[0];
                const modelAttributes = getModelAttributes("bookshelf", model);
                const boardsNumber = parseInt(bayModels[1]);

                const direction = calculateDirection(bayAttributes);

                const leftDividerWidth = bayColumn === 0 ? modelAttributes.side_dividers_width : modelAttributes.central_dividers_width;
                const rightDividerWidth = column === splitModels.length ? modelAttributes.side_dividers_width : modelAttributes.central_dividers_width;

                let startLinePos = (modelAttributes.width - leftDividerWidth - rightDividerWidth) * startLinePosition / 100;
                let endLinePos = (modelAttributes.width - leftDividerWidth - rightDividerWidth) * endLinePosition / 100;

                if (modelAttributes) {
                    const accumulatedWidth = accumulatedModelsWidth(bayAttributes.models, bayColumn);
                    const posX = starting_position.x + (accumulatedWidth + modelAttributes.width / 2) * direction;

                    const basePosY = starting_position.y + modelAttributes.height / 2 + modelAttributes.lower_plinth_height / 2 - epsilon / 2 + modelAttributes.wheel_diameter + modelAttributes.middle_boards_height / 2;

                    for (let line = (column === start_column ? startLine : 1); line <= (column === end_column ? endLine : boardsNumber); line++) {
                        const posY = basePosY + modelAttributes.height / 2 - (modelAttributes.height / boardsNumber) * (line - 1) - (modelAttributes.height / boardsNumber) / 2;
                        const sectionHeight = (modelAttributes.height / boardsNumber) * 0.75;

                        const isStart = column === start_column && line === startLine;
                        const isEnd = column === end_column && line === endLine;

                        let meshPositionX, meshScaleX;

                        if (sameColumnAndLine) {
                            meshPositionX = starting_position.x + accumulatedWidth * direction + startLinePos * direction + (endLinePos - startLinePos) / 2 * direction;
                            meshScaleX = endLinePos - startLinePos;
                        } else {
                            if (isEnd) {
                                meshPositionX = posX + (direction === -1 ? (modelAttributes.width / 2 - endLinePos / 2) : (endLinePos / 2 - modelAttributes.width / 2)) + leftDividerWidth * direction;
                            } else if (isStart) {
                                meshPositionX = posX + (direction === -1 ? (-startLinePos / 2) : (startLinePos / 2));
                                meshPositionX += (leftDividerWidth / 2 - rightDividerWidth / 2) * direction;
                            } else {
                                meshPositionX = posX;
                                meshPositionX += (leftDividerWidth / 2 - rightDividerWidth / 2) * direction;
                            }

                            meshScaleX = isEnd ? endLinePos : modelAttributes.width - (isStart ? startLinePos : 0);
                        }

                        meshScaleX = isEnd ? meshScaleX : meshScaleX - leftDividerWidth - rightDividerWidth;
                        meshScaleX = Math.max(0, meshScaleX);

                        const textTexture = createTextTexture(section.call_number, section.name, meshScaleX * 1000, sectionHeight * 1000);
                        const textMaterial = new MeshBasicMaterial({ map: textTexture, transparent: true, side: FrontSide });

                        const textGeometry = new BoxGeometry(meshScaleX - epsilon, sectionHeight - epsilon, 0.001);
                        textGeometry.applyMatrix4(new Matrix4().makeTranslation(0, 0, modelAttributes.depth / 2 * direction + epsilon));
                        const textMesh = new BufferGeometry();
                        textMesh.copy(textGeometry);
                        textMesh.applyMatrix4(new Matrix4().makeTranslation(meshPositionX, posY, starting_position.z));

                        textMaterials.push({ geometry: textMesh, material: textMaterial });

                        const booksNumber = Math.floor((meshScaleX - epsilon) / (bookWidth + bookSpacing));
                        const totalBookWidth = booksNumber * (bookWidth + bookSpacing) - bookSpacing;

                        for (let bookIndex = 0; bookIndex < booksNumber; bookIndex++) {
                            const bookPosX = meshPositionX + bookIndex * (bookWidth + bookSpacing) - totalBookWidth / 2 + bookWidth / 2;

                            const bookGeometry = new BoxGeometry(bookWidth, sectionHeight, modelAttributes.depth * 0.9);
                            const book = new BufferGeometry();
                            book.copy(bookGeometry);
                            book.applyMatrix4(new Matrix4().makeTranslation(bookPosX, posY, starting_position.z + epsilon));

                            bookGeometries.push(book);

                            const bookEdgesGeometry = new EdgesGeometry(bookGeometry);
                            const bookEdges = new BufferGeometry();
                            bookEdges.copy(bookEdgesGeometry);
                            bookEdges.applyMatrix4(new Matrix4().makeTranslation(bookPosX, posY, starting_position.z + epsilon));

                            edgesGeometries.push(bookEdges);
                        }
                    }
                } else {
                    console.error(`Le modèle ${model} pour le type bookshelf n'a pas été trouvé.`);
                }
            } else {
                console.error(`Pas de modèle de bookshelf pour la colonne ${column} de la bayAttributes ${bayAttributes.id_bay}.`);
            }
        }

        const mergedBookGeometry = BufferGeometryUtils.mergeGeometries(bookGeometries);
        const mergedEdgesGeometry = BufferGeometryUtils.mergeGeometries(edgesGeometries);

        setSectionsMeshes({
            call_number: section.call_number,
            name: section.name,
            sectionMaterial: sectionMaterial,
            bookGeometry: mergedBookGeometry,
            bookMaterial: bookMaterial,
            edgesGeometry: mergedEdgesGeometry,
            edgesMaterial: edgesMaterial,
            textMaterials: textMaterials
        });

        return () => {
            mergedBookGeometry.dispose();
            mergedEdgesGeometry.dispose();
            textMaterials.forEach(({ material }) => material.map.dispose());
        };
    }, [bayData, section, isPreview]);

    useEffect(() => {
        if (sectionsMeshes) {
            setSectionInformation(prev => [...prev, section]);
        }
    }, [sectionsMeshes, setSectionInformation, section]);

    return (
        <>
            {sectionsMeshes && (
                <>
                    <mesh
                        geometry={sectionsMeshes.bookGeometry}
                        material={sectionsMeshes.bookMaterial}
                    />
                    <lineSegments
                        geometry={sectionsMeshes.edgesGeometry}
                        material={sectionsMeshes.edgesMaterial}
                    />
                    {sectionsMeshes.textMaterials.map(({ geometry, material }, index) => (
                        <mesh
                            key={index}
                            geometry={geometry}
                            material={material}
                        />
                    ))}
                </>
            )}
        </>
    );
}
