import { useMemo } from 'react';
import {  } from '../../../../utils/dataUtils.js';
import Section from './Section.jsx';

export default function Sections({ sections, isPreview = false, libraryId }) 
{
  return (
    <>
      {sections.map((section, index) => (
        <Section key={index} section={section} isPreview={isPreview} libraryId={libraryId} />
      ))}
    </>
  );
}
