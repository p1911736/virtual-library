import React, { useState, useEffect, useMemo } from 'react';
import axios from 'axios';
import { BufferGeometryUtils } from 'three/examples/jsm/Addons.js';
import { Vector3 } from 'three';
import Window from './Window.jsx';
import { getModelAttributes } from '../../../utils/dataUtils.js';
import { calculateDirection } from '../../../utils/calculateUtils.js';
import { useGlobalContextLibrary } from '../../../interfaces/VirtualLibrary/VirtualLibraryContext.jsx';
import ApiService from '../../../api/ApiService.js';

export default function Windows() {
  const [windowData, setWindowData] = useState(null);
  const { libraryInfo } = useGlobalContextLibrary();

  useEffect(() => {
    if (libraryInfo.id) {
      axios.get(`${ApiService.getAPI_URL()}/api/windows/${libraryInfo.id}`)
        .then(response => {
          setWindowData(response.data);
        })
        .catch(error => {
          console.error('Error fetching windows JSON:', error);
        });
    }
  }, [libraryInfo]);

  const { geometries, materials } = useMemo(() => {
    if (!windowData) return { geometries: new Map(), materials: new Map() };

    const mergedGeometries = new Map();
    const materials = new Map();

    windowData.forEach((window, index) => {
      const { models, starting_position } = window;
      
      const splitModels = Array.from(models.matchAll(/([A-Z])(\d*(?:\.\d+)?)/g), match => [match[1], match[2] || ""]);
      const modelIdentifier = splitModels[0][0];
      const modelAttributes = getModelAttributes("window", modelIdentifier);
      const height_elevation = parseFloat(splitModels[1][1]);

      let line = 1;
      let line_spacement = 0;
      let column = 1;
      let column_spacement = 0;

      if (splitModels.length > 2) {
        line = parseInt(splitModels[2][1]);
        line_spacement = parseFloat(splitModels[3][1]);
        column = parseInt(splitModels[4][1]);
        column_spacement = parseFloat(splitModels[5][1]);
      }

      let currentY = starting_position.y + height_elevation;
      let currentZ = starting_position.z;
      const direction = calculateDirection(window);

      const modelWidth = modelAttributes.width;
      const modelHeight = modelAttributes.height;

      for (let i = 0; i < column; i++) {
        let currentX = starting_position.x + modelWidth / 2;

        for (let j = 0; j < line; j++) {
          const position = new Vector3(
            currentX * direction,
            currentY,
            currentZ
          );

          const { geometry, material } = Window({ position, windowModelData: modelAttributes });

          if (!mergedGeometries.has(modelIdentifier)) {
            mergedGeometries.set(modelIdentifier, []);
            materials.set(modelIdentifier, material);
          }

          mergedGeometries.get(modelIdentifier).push(geometry);

          currentX += line_spacement + modelWidth;
        }

        currentY += column_spacement + modelHeight;
      }
    });

    return { geometries: mergedGeometries, materials };
  }, [windowData]);

  const mergedGeometries = useMemo(() => {
    const merged = {};
    geometries.forEach((geometryArray, modelIdentifier) => {
      merged[modelIdentifier] = BufferGeometryUtils.mergeGeometries(geometryArray);
    });
    return merged;
  }, [geometries]);

  return (
    <>
      {Object.keys(mergedGeometries).map((modelIdentifier) => (
        <mesh key={modelIdentifier} geometry={mergedGeometries[modelIdentifier]} material={materials.get(modelIdentifier)} />
      ))}
    </>
  );
}
