import { BoxGeometry, MeshPhysicalMaterial } from 'three';

const boxGeometry = new BoxGeometry();

export default function Window({ windowModelData, position }) 
{
  const { width, height, depth, furniture_material } = windowModelData;

  const geometry = new BoxGeometry(width, height, depth);
  geometry.translate(position.x, position.y, position.z);

  const material = new MeshPhysicalMaterial({ color: furniture_material });

  return { geometry, material };
}
