import React, { useState, useEffect } from 'react';
import { BoxGeometry, MeshPhysicalMaterial } from 'three';
import { BufferGeometryUtils } from 'three/examples/jsm/Addons.js';
import { getModelAttributes } from '../../utils/dataUtils.js';
import { calculateAttributesBetweenPoints, convertCoordinatesToVector3 } from '../../utils/calculateUtils.js';
import { useGlobalContextLibrary } from '../../interfaces/VirtualLibrary/VirtualLibraryContext.jsx';
import axios from 'axios';
import ApiService from '../../api/ApiService.js';

export default function Walls() {
  const [wallData, setWallData] = useState([]);
  const [geometries, setGeometries] = useState({});
  const [materials, setMaterials] = useState({});
  const { libraryInfo } = useGlobalContextLibrary();

  // Charger les données des murs
  useEffect(() => {
    if (libraryInfo.id) {
      axios.get(`${ApiService.getAPI_URL()}/api/walls/${libraryInfo.id}`)
        .then(response => {
          setWallData(response.data);
        })
        .catch(error => {
          console.error('Error fetching walls JSON:', error);
        });
    }
  }, [libraryInfo]);

  // Calculer les géométries et matériaux lorsque wallData change
  useEffect(() => {
    if (wallData.length > 0) {
      const convertedData = convertCoordinatesToVector3(wallData);

      const allGeometries = {};
      const newMaterials = {};

      convertedData.forEach((wall) => {
        const modelAttributes = getModelAttributes('wall', wall.model);
        const { thickness, height, furniture_material } = modelAttributes;

        if (!allGeometries[furniture_material]) {
          allGeometries[furniture_material] = [];
          newMaterials[furniture_material] = new MeshPhysicalMaterial({ color: furniture_material });
        }

        wall.coordinates.forEach((pointA, index) => {
          const pointB = wall.coordinates[(index + 1) % wall.coordinates.length];
          const { length, position, rotation } = calculateAttributesBetweenPoints(pointA, pointB, height);

          const boxGeometry = new BoxGeometry(thickness, height, length + thickness);
          boxGeometry.rotateY(rotation.y);
          boxGeometry.translate(position.x, position.y, position.z);

          allGeometries[furniture_material].push(boxGeometry);
        });
      });

      const mergedGeometries = Object.keys(allGeometries).reduce((acc, materialKey) => {
        acc[materialKey] = BufferGeometryUtils.mergeGeometries(allGeometries[materialKey]);
        return acc;
      }, {});

      setGeometries(mergedGeometries);
      setMaterials(newMaterials);
    }
  }, [wallData]);

  // Affichage des meshes lorsque les géométries sont prêtes
  return (
    <>
      {Object.keys(geometries).map((materialKey) => (
        <mesh key={materialKey} geometry={geometries[materialKey]} material={materials[materialKey]} />
      ))}
    </>
  );
}
