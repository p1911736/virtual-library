const API_URL = 'http://localhost:5172';

const ApiService = {
  getAPI_URL() {
    if (window.location.href.startsWith("https://")) {
      return API_URL.replace("http://", "https://");
    }

    return API_URL;
  }
};

export default ApiService;
