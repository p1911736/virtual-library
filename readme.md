Code crée avec React JS et React three fiber.

# Dossiers

/src : code source.
    /api : api dont la base de l'url.
    /components : composants React.
        /Configuration : 
        /Forms : tout ce qui est lié aux formulaires.
        /Interactions : 
        /Interfaces : interfaces html sur la bibliothèque virtuelle.
            - Interface globale : nom de la bibliothèque, icônes, étages...
            - Interface du livre : affichage des informations du livre (titre, image, auteur...).
        /Itinerary : tout ce qui est lié à l'itinéraire vers un livre.
        /Structure : objets 3D (étagères, fenêtres, carrels, sol, murs, piliers...).
        /Texts : tout ce qui est lié aux textes (numéro et nom des étages).
    /css : styles.
    /interfaces : différentes interfaces (bibliothèque virtuelle, édition des sections, édition des modèles).
        /!\ Attention : les interfaces d'édition des sections et des modèles ne sont pas terminées.
    /json : fichiers json contenant toutes les données.
        /fieldsModels : champs de formulaires pour l'édition des modèles pour la création de formulaires de manière dynamique (DynamicForm).
        /fieldsSections : champs de formulaires pour l'édition des sections pour la création de formulaires de manière dynamique (DynamicForm).
        /libraries : données des bibliothèques.
            /library_id : contient les données d'une bibliothèque comme les objets 3D (leurs positions, dimensions, matériaux...) et leurs sections. 
        /models : modèles d'objets 3D.
    /utils : fichiers js contenant toutes les fonctions.
/public : ressources (polices d'écriture, images...).


# Remarques

- L'id de la bibliothèque (qui permet de savoir quelle bibliothèque visualiser) est stocké dans une variable global (useContext).
- Les objets 3D sont affichés en fonction d'un modèle. Un modèle contient des informations sur l'objet comme ses dimensions, son matériau...
- Les positions de la caméra pour les boutons pour chaqua étage (vue réelle et d'ensemble) sont à modifier dans `floors.json`.


# Perspectives

- Ajout de la modélisation 3D des escaliers.
- Terminer l'édition de sections et de modèles.
- Ajout des fenêtres de la BU sur JOSM.
- Ajout des portes de la BU sur JOSM + modélisation en 3D.
- Faire que la hauteur de la caméra soit déterminée par le paramètre taille de l'utilisateur dans OPALE.
- Récupérer les informations d'un livre sur sherlock.
- Recommandation d'autres livres si un n'est plus disponible.
- Ajout de la modélisation 3D des tables + itinéraire.
- Bouton voir/sortir de la section qui offre une vue rapprochée sur la section dans la travée.
- Améliorer la modélisation des roulettes d'une étagère.
- Visuel pour le sens des lectures d'une section.

# Mise en production