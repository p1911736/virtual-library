import express from 'express';
import bodyParser from 'body-parser';
import route from './src/utils/requests.js';
import cors from 'cors';

const app = express();
const backendPort = 5172;
const frontendPort = 5173;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.json());

const corsOptions = {
    origin: `http://localhost:${frontendPort}`,
    optionsSuccessStatus: 200,
    credentials: true
};
app.use(cors(corsOptions));

app.use('/api', route);

app.listen(backendPort, () => {
    console.log(`Server is running on http://localhost:${backendPort}`);
});
